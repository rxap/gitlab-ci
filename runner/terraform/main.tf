provider "google" {
  version = "3.68.0"

  credentials = file("<NAME>.json")

  project = "this-one-app"
  region  = "europe-west3"
  zone    = "europe-west3-c"
}

resource "google_container_cluster" "ci" {
  name     = "ci"
  location = "europe-west3"

  # IMPORTANT: to ignore changes from separately-managed node pools
  lifecycle {
    ignore_changes = [node_pool]
  }

  addons_config {
    # unused feature, just disable
    http_load_balancing {
      disabled = true
    }
  }

  network_policy {
    enabled  = true
    provider = "CALICO"
  }

  node_pool {
    name = "default-pool"
  }
}

resource "google_container_node_pool" "gitlab_runner" {
  autoscaling {
    min_node_count = 0
    max_node_count = 4
  }
  node_config {
    machine_type    = "n1-standard-1"
    preemptible     = true
  }
  cluster = google_container_cluster.ci.name
  location = "europe-west3"
}

resource "google_storage_bucket" "gitlab_runner" {
  name     = "this-one-gitlab-ci-cache"
  location = "europe-west3"
}

resource "google_storage_bucket_iam_binding" "gitlab_runner" {
  bucket   = google_storage_bucket.gitlab_runner.name
  role     = "roles/storage.objectAdmin"

  members = [
    "serviceAccount:${google_service_account.gke.email}",
  ]
}

resource "google_service_account" "gke" {
  account_id   = "gitlab-ci-runner"
  display_name = "service account to access gcs bucket"
}
