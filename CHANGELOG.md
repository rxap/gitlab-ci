# Changelog

## [6.3.4-nightly.2](https://gitlab.com/rxap/gitlab-ci/compare/6.3.4-nightly.1...6.3.4-nightly.2) (2025-03-04)

## [6.3.4-edge.5](https://gitlab.com/rxap/gitlab-ci/compare/6.3.4-nightly.1...6.3.4-nightly.2) (2025-03-02)


### Bug Fixes

* remove default custom platform ([6687ec2](https://gitlab.com/rxap/gitlab-ci/commit/6687ec2c103aeff9c2525a3373ebe675754400ec))

## [6.3.4-edge.4](https://gitlab.com/rxap/gitlab-ci/compare/6.3.4-nightly.1...6.3.4-nightly.2) (2025-03-02)

## [6.3.4-edge.5](https://gitlab.com/rxap/gitlab-ci/compare/6.3.4-edge.4...6.3.4-edge.5) (2025-03-02)


### Bug Fixes

* remove default custom platform ([6687ec2](https://gitlab.com/rxap/gitlab-ci/commit/6687ec2c103aeff9c2525a3373ebe675754400ec))

## [6.3.4-edge.4](https://gitlab.com/rxap/gitlab-ci/compare/6.3.4-edge.3...6.3.4-edge.4) (2025-03-02)


### Bug Fixes

* remove default custom platform ([c75a17b](https://gitlab.com/rxap/gitlab-ci/commit/c75a17b68299cefdb93c4845dfe1e8b6a7cd1ea4))
* set to edge ([73bbab3](https://gitlab.com/rxap/gitlab-ci/commit/73bbab3643fbe05f2bc59700539d571619baf658))

## [6.3.4-nightly.1](https://gitlab.com/rxap/gitlab-ci/compare/6.3.4-nightly.0...6.3.4-nightly.1) (2025-02-20)

## [6.3.4-edge.3](https://gitlab.com/rxap/gitlab-ci/compare/6.3.4-nightly.0...6.3.4-nightly.1) (2025-02-19)


### Bug Fixes

* pin to specific git version ([7412d29](https://gitlab.com/rxap/gitlab-ci/commit/7412d29086ceb51f032c22c8a46e30dbe5bc888f))
* support arg --custom-platform ([43d82f9](https://gitlab.com/rxap/gitlab-ci/commit/43d82f94d05bfe4aa3c927ce7b59cce02858f418))

## [6.3.4-edge.3](https://gitlab.com/rxap/gitlab-ci/compare/6.3.4-edge.2...6.3.4-edge.3) (2025-02-19)


### Bug Fixes

* pin to specific git version ([7412d29](https://gitlab.com/rxap/gitlab-ci/commit/7412d29086ceb51f032c22c8a46e30dbe5bc888f))
* support arg --custom-platform ([43d82f9](https://gitlab.com/rxap/gitlab-ci/commit/43d82f94d05bfe4aa3c927ce7b59cce02858f418))

## [6.3.4-edge.2](https://gitlab.com/rxap/gitlab-ci/compare/6.3.4-edge.1...6.3.4-edge.2) (2025-02-19)


### Bug Fixes

* remove the platform arg ([34343f3](https://gitlab.com/rxap/gitlab-ci/commit/34343f3b132ab72b3377a66e9cf5c463228b508e))

## [6.3.4-edge.1](https://gitlab.com/rxap/gitlab-ci/compare/6.3.4-edge.0...6.3.4-edge.1) (2025-02-19)


### Bug Fixes

* support multi platform builds ([78c6043](https://gitlab.com/rxap/gitlab-ci/commit/78c6043fa9f0dcdc1dbc42530e152b504aa55cec))

## [6.3.4-nightly.0](https://gitlab.com/rxap/gitlab-ci/compare/6.3.4-edge.0...6.3.4-edge.1) (2025-02-19)

## [6.3.4-nightly.0](https://gitlab.com/rxap/gitlab-ci/compare/6.3.3...6.3.4-nightly.0) (2025-02-19)

## [6.3.4-edge.0](https://gitlab.com/rxap/gitlab-ci/compare/6.3.3...6.3.4-nightly.0) (2025-02-18)


### Bug Fixes

* remove default docker tags ([e860290](https://gitlab.com/rxap/gitlab-ci/commit/e8602905d71b4d7233094216ebda2b94eb729306))

## [6.3.4-edge.0](https://gitlab.com/rxap/gitlab-ci/compare/6.3.3...6.3.4-edge.0) (2025-02-18)


### Bug Fixes

* remove default docker tags ([e860290](https://gitlab.com/rxap/gitlab-ci/commit/e8602905d71b4d7233094216ebda2b94eb729306))

## [6.3.3](https://gitlab.com/rxap/gitlab-ci/compare/6.3.2...6.3.3) (2025-02-18)


### Bug Fixes

* set retry to limit of 2 ([d397b69](https://gitlab.com/rxap/gitlab-ci/commit/d397b6936978bff22a7fd72096dfc4c9f370606c))

## [6.3.2](https://gitlab.com/rxap/gitlab-ci/compare/6.3.1...6.3.2) (2025-02-18)

## [6.3.2-edge.3](https://gitlab.com/rxap/gitlab-ci/compare/6.3.1...6.3.2) (2025-02-18)


### Bug Fixes

* retry on nx install failure ([f6387b8](https://gitlab.com/rxap/gitlab-ci/commit/f6387b85499ef5091ce8bfc98d5b2e7bf2ecefe7))

## [6.3.2-edge.2](https://gitlab.com/rxap/gitlab-ci/compare/6.3.1...6.3.2) (2025-02-18)

## [6.3.2-edge.3](https://gitlab.com/rxap/gitlab-ci/compare/6.3.2-edge.2...6.3.2-edge.3) (2025-02-18)


### Bug Fixes

* retry on nx install failure ([f6387b8](https://gitlab.com/rxap/gitlab-ci/commit/f6387b85499ef5091ce8bfc98d5b2e7bf2ecefe7))

## [6.3.2-edge.2](https://gitlab.com/rxap/gitlab-ci/compare/6.3.2-edge.1...6.3.2-edge.2) (2025-02-18)


### Bug Fixes

* add pages needs input ([3fa25ea](https://gitlab.com/rxap/gitlab-ci/commit/3fa25eafcedebaaf3c92e52c717c1b10495c8a8e))

## [6.3.2-edge.1](https://gitlab.com/rxap/gitlab-ci/compare/6.3.2-edge.0...6.3.2-edge.1) (2025-02-18)


### Bug Fixes

* load docs from workspace root ([ecee8e6](https://gitlab.com/rxap/gitlab-ci/commit/ecee8e6000aaf73d8eef03ea11f04b6f3a13bbd4))

## [6.3.2-nightly.0](https://gitlab.com/rxap/gitlab-ci/compare/6.3.2-edge.0...6.3.2-edge.1) (2025-02-18)

## [6.3.2-nightly.0](https://gitlab.com/rxap/gitlab-ci/compare/6.3.1...6.3.2-nightly.0) (2025-02-18)

## [6.3.2-edge.0](https://gitlab.com/rxap/gitlab-ci/compare/6.3.1...6.3.2-nightly.0) (2025-02-17)


### Bug Fixes

* print the git commit hash ([c158507](https://gitlab.com/rxap/gitlab-ci/commit/c1585079360ba0f024f45a88f92a254acb60bb6b))
* print the git commit hash ([9b84c2b](https://gitlab.com/rxap/gitlab-ci/commit/9b84c2bd9143a64c2d5006f223ccfbeb4afd910f))

## [6.3.2-edge.0](https://gitlab.com/rxap/gitlab-ci/compare/6.3.1...6.3.2-edge.0) (2025-02-17)


### Bug Fixes

* disable cache for apindex and git images ([70b6ff1](https://gitlab.com/rxap/gitlab-ci/commit/70b6ff1ad409ba203cdfb9b7eb8b53930bfd5df8))
* print the git commit hash ([c158507](https://gitlab.com/rxap/gitlab-ci/commit/c1585079360ba0f024f45a88f92a254acb60bb6b))
* print the git commit hash ([9b84c2b](https://gitlab.com/rxap/gitlab-ci/commit/9b84c2bd9143a64c2d5006f223ccfbeb4afd910f))

## [6.3.1](https://gitlab.com/rxap/gitlab-ci/compare/6.3.0...6.3.1) (2025-02-17)

## [6.3.1-edge.2](https://gitlab.com/rxap/gitlab-ci/compare/6.3.0...6.3.1) (2025-02-17)


### Bug Fixes

* only execute the build target by default ([7ab0130](https://gitlab.com/rxap/gitlab-ci/commit/7ab01304b880f9c22039dca4297e5543540a5232))

## [6.3.1-edge.1](https://gitlab.com/rxap/gitlab-ci/compare/6.3.0...6.3.1) (2025-02-17)

## [6.3.1-edge.2](https://gitlab.com/rxap/gitlab-ci/compare/6.3.1-edge.1...6.3.1-edge.2) (2025-02-17)


### Bug Fixes

* only execute the build target by default ([7ab0130](https://gitlab.com/rxap/gitlab-ci/commit/7ab01304b880f9c22039dca4297e5543540a5232))

## [6.3.1-edge.1](https://gitlab.com/rxap/gitlab-ci/compare/6.3.1-edge.0...6.3.1-edge.1) (2025-02-17)


### Bug Fixes

* add missing input pass through ([f3ee115](https://gitlab.com/rxap/gitlab-ci/commit/f3ee115acf368d12d69c1b8e764ed335c7140d82))
* install bash and cmake by default ([2f1d969](https://gitlab.com/rxap/gitlab-ci/commit/2f1d969b068986918273c3c2cc6ddc4fa62c5080))
* install wget by default ([a00bb7c](https://gitlab.com/rxap/gitlab-ci/commit/a00bb7c7420f360a6eae579ea7ec557958bb1144))

## [6.3.1-edge.0](https://gitlab.com/rxap/gitlab-ci/compare/6.3.0...6.3.1-edge.0) (2025-02-17)


### Bug Fixes

* cleanup the target directory ([4371c96](https://gitlab.com/rxap/gitlab-ci/commit/4371c96364da0c29ea187e118f15d4f69cf88dde))
* copy the docs folder ([bb328d1](https://gitlab.com/rxap/gitlab-ci/commit/bb328d1bfe323b022ec398dcd149a543bfc3f40b))
* only run the build target by default ([1300481](https://gitlab.com/rxap/gitlab-ci/commit/1300481f93c13862650a64281ed700f3a42e2ea5))

## [6.3.0](https://gitlab.com/rxap/gitlab-ci/compare/6.2.6...6.3.0) (2025-02-11)

## [6.2.7-nightly.1](https://gitlab.com/rxap/gitlab-ci/compare/6.2.6...6.3.0) (2025-02-08)

## [6.3.0-edge.35](https://gitlab.com/rxap/gitlab-ci/compare/6.2.6...6.3.0) (2025-02-07)


### Bug Fixes

* simplify env definition ([378d90b](https://gitlab.com/rxap/gitlab-ci/commit/378d90b6b23c1e783ed90ce0d4a2f2edd03b2863))

## [6.2.7-nightly.1](https://gitlab.com/rxap/gitlab-ci/compare/6.2.7-nightly.0...6.2.7-nightly.1) (2025-02-08)

## [6.3.0-edge.35](https://gitlab.com/rxap/gitlab-ci/compare/6.2.7-nightly.0...6.2.7-nightly.1) (2025-02-07)


### Bug Fixes

* simplify env definition ([378d90b](https://gitlab.com/rxap/gitlab-ci/commit/378d90b6b23c1e783ed90ce0d4a2f2edd03b2863))

## [6.3.0-edge.35](https://gitlab.com/rxap/gitlab-ci/compare/6.3.0-edge.34...6.3.0-edge.35) (2025-02-07)


### Bug Fixes

* simplify env definition ([378d90b](https://gitlab.com/rxap/gitlab-ci/commit/378d90b6b23c1e783ed90ce0d4a2f2edd03b2863))

## [6.2.7-nightly.0](https://gitlab.com/rxap/gitlab-ci/compare/6.3.0-edge.34...6.3.0-edge.35) (2025-02-04)

## [6.2.7-nightly.0](https://gitlab.com/rxap/gitlab-ci/compare/6.2.6...6.2.7-nightly.0) (2025-02-04)

## [6.3.0-edge.34](https://gitlab.com/rxap/gitlab-ci/compare/6.2.6...6.2.7-nightly.0) (2025-02-03)


### Features

* support dynamic env.yaml files ([7c8f620](https://gitlab.com/rxap/gitlab-ci/commit/7c8f620ef9c92f9564bbce303a95c2d929f6c817))

## [6.3.0-edge.34](https://gitlab.com/rxap/gitlab-ci/compare/6.3.0-edge.33...6.3.0-edge.34) (2025-02-03)


### Features

* support dynamic env.yaml files ([7c8f620](https://gitlab.com/rxap/gitlab-ci/commit/7c8f620ef9c92f9564bbce303a95c2d929f6c817))

## [6.2.6](https://gitlab.com/rxap/gitlab-ci/compare/6.3.0-edge.33...6.3.0-edge.34) (2025-01-29)

## [6.2.6](https://gitlab.com/rxap/gitlab-ci/compare/6.2.5...6.2.6) (2025-01-29)

## [6.3.0-edge.33](https://gitlab.com/rxap/gitlab-ci/compare/6.2.5...6.2.6) (2025-01-29)


### Features

* introduce GCP_IMAGE_NAME ([589dd82](https://gitlab.com/rxap/gitlab-ci/commit/589dd8204b13497d35e1b3b598981596548707ac))

## [6.2.6-nightly.3](https://gitlab.com/rxap/gitlab-ci/compare/6.2.5...6.2.6) (2025-01-29)

## [6.3.0-edge.33](https://gitlab.com/rxap/gitlab-ci/compare/6.3.0-edge.32...6.3.0-edge.33) (2025-01-29)


### Features

* introduce GCP_IMAGE_NAME ([589dd82](https://gitlab.com/rxap/gitlab-ci/commit/589dd8204b13497d35e1b3b598981596548707ac))

## [6.2.6-nightly.3](https://gitlab.com/rxap/gitlab-ci/compare/6.3.0-edge.32...6.3.0-edge.33) (2025-01-29)

## [6.2.6-nightly.3](https://gitlab.com/rxap/gitlab-ci/compare/6.2.6-nightly.2...6.2.6-nightly.3) (2025-01-29)

## [6.3.0-edge.32](https://gitlab.com/rxap/gitlab-ci/compare/6.2.6-nightly.2...6.2.6-nightly.3) (2025-01-28)


### Bug Fixes

* support nx workspace with default project ([7eb7e9c](https://gitlab.com/rxap/gitlab-ci/commit/7eb7e9c202f0c8c1ef69e377f1cfc3cd683bfcb2))

## [6.3.0-edge.31](https://gitlab.com/rxap/gitlab-ci/compare/6.2.6-nightly.2...6.2.6-nightly.3) (2025-01-28)

## [6.3.0-edge.32](https://gitlab.com/rxap/gitlab-ci/compare/6.3.0-edge.31...6.3.0-edge.32) (2025-01-28)


### Bug Fixes

* support nx workspace with default project ([7eb7e9c](https://gitlab.com/rxap/gitlab-ci/commit/7eb7e9c202f0c8c1ef69e377f1cfc3cd683bfcb2))

## [6.3.0-edge.31](https://gitlab.com/rxap/gitlab-ci/compare/6.3.0-edge.30...6.3.0-edge.31) (2025-01-28)


### Bug Fixes

* use unique name for service account key file env ([ab6ab15](https://gitlab.com/rxap/gitlab-ci/commit/ab6ab152efb87f2f4dc9393a6da542b36b3429d1))

## [6.2.6-nightly.2](https://gitlab.com/rxap/gitlab-ci/compare/6.3.0-edge.30...6.3.0-edge.31) (2025-01-28)

## [6.2.6-nightly.2](https://gitlab.com/rxap/gitlab-ci/compare/6.2.6-nightly.1...6.2.6-nightly.2) (2025-01-28)

## [6.3.0-edge.30](https://gitlab.com/rxap/gitlab-ci/compare/6.2.6-nightly.1...6.2.6-nightly.2) (2025-01-27)


### Bug Fixes

* add missing region flag ([ecf3c61](https://gitlab.com/rxap/gitlab-ci/commit/ecf3c618f281872b51b6fd008991910bf28035d0))

## [6.3.0-edge.29](https://gitlab.com/rxap/gitlab-ci/compare/6.2.6-nightly.1...6.2.6-nightly.2) (2025-01-27)

## [6.3.0-edge.30](https://gitlab.com/rxap/gitlab-ci/compare/6.3.0-edge.29...6.3.0-edge.30) (2025-01-27)


### Bug Fixes

* add missing region flag ([ecf3c61](https://gitlab.com/rxap/gitlab-ci/commit/ecf3c618f281872b51b6fd008991910bf28035d0))

## [6.3.0-edge.29](https://gitlab.com/rxap/gitlab-ci/compare/6.3.0-edge.28...6.3.0-edge.29) (2025-01-27)


### Bug Fixes

* only run to-tag manual ([b2a8477](https://gitlab.com/rxap/gitlab-ci/commit/b2a84777301651ff1e2a6e8aa2fd04e5e50cbffe))
* support needs configuration ([46676e1](https://gitlab.com/rxap/gitlab-ci/commit/46676e1b969f908b3abf1fd7fd20c02a1dd44227))

## [6.3.0-edge.28](https://gitlab.com/rxap/gitlab-ci/compare/6.3.0-edge.27...6.3.0-edge.28) (2025-01-27)


### Bug Fixes

* add missing component divider ([e3735a9](https://gitlab.com/rxap/gitlab-ci/commit/e3735a9b3dbff547ec13537ad5848584dc64ad32))
* use proper inputs ([2aed75a](https://gitlab.com/rxap/gitlab-ci/commit/2aed75a4df74435f60ad00378786c83b1e793dfc))

## [6.3.0-edge.27](https://gitlab.com/rxap/gitlab-ci/compare/6.3.0-edge.26...6.3.0-edge.27) (2025-01-27)


### Bug Fixes

* add dynamic cloud sdk version ([1ee17c7](https://gitlab.com/rxap/gitlab-ci/commit/1ee17c7be4d9e4645b010e9e5d2c2fbf7d6cdff4))

## [6.3.0-edge.26](https://gitlab.com/rxap/gitlab-ci/compare/6.3.0-edge.25...6.3.0-edge.26) (2025-01-27)


### Bug Fixes

* fix env setup ([3269472](https://gitlab.com/rxap/gitlab-ci/commit/32694725474b881a96375e875c3e076cac66e607))

## [6.3.0-edge.25](https://gitlab.com/rxap/gitlab-ci/compare/6.3.0-edge.24...6.3.0-edge.25) (2025-01-27)


### Features

* add gcloud jobs ([4e1c871](https://gitlab.com/rxap/gitlab-ci/commit/4e1c87127598e7d988763d2a70dd610918ab04d3))
* include base by default ([9d597f8](https://gitlab.com/rxap/gitlab-ci/commit/9d597f808d6c198dc69e004dc13545004b7a1316))

## [6.2.6-nightly.1](https://gitlab.com/rxap/gitlab-ci/compare/6.3.0-edge.24...6.3.0-edge.25) (2025-01-25)

## [6.2.6-nightly.1](https://gitlab.com/rxap/gitlab-ci/compare/6.2.6-nightly.0...6.2.6-nightly.1) (2025-01-25)

## [6.3.0-edge.24](https://gitlab.com/rxap/gitlab-ci/compare/6.2.6-nightly.0...6.2.6-nightly.1) (2025-01-24)


### Bug Fixes

* support json and base64 key file ([b9afa97](https://gitlab.com/rxap/gitlab-ci/commit/b9afa9749f2f5d1783eded8dbba4914ebac26cbb))

## [6.3.0-edge.24](https://gitlab.com/rxap/gitlab-ci/compare/6.3.0-edge.23...6.3.0-edge.24) (2025-01-24)


### Bug Fixes

* support json and base64 key file ([b9afa97](https://gitlab.com/rxap/gitlab-ci/commit/b9afa9749f2f5d1783eded8dbba4914ebac26cbb))

## [6.2.6-nightly.0](https://gitlab.com/rxap/gitlab-ci/compare/6.3.0-edge.23...6.3.0-edge.24) (2025-01-23)

## [6.2.6-nightly.0](https://gitlab.com/rxap/gitlab-ci/compare/6.2.5...6.2.6-nightly.0) (2025-01-23)

## [6.3.0-edge.23](https://gitlab.com/rxap/gitlab-ci/compare/6.2.5...6.2.6-nightly.0) (2025-01-22)


### Bug Fixes

* update default node version to iron ([e72c6bc](https://gitlab.com/rxap/gitlab-ci/commit/e72c6bc20356f654df99c039fa28dce7148bb8d8))

## [6.3.0-edge.23](https://gitlab.com/rxap/gitlab-ci/compare/6.3.0-edge.22...6.3.0-edge.23) (2025-01-22)


### Bug Fixes

* update default node version to iron ([e72c6bc](https://gitlab.com/rxap/gitlab-ci/commit/e72c6bc20356f654df99c039fa28dce7148bb8d8))

## [6.3.0-edge.22](https://gitlab.com/rxap/gitlab-ci/compare/6.3.0-edge.21...6.3.0-edge.22) (2025-01-22)


### Bug Fixes

* remove default tags ([d0a3a3f](https://gitlab.com/rxap/gitlab-ci/commit/d0a3a3f94132c23c4bce75e38e8632431a50c529))
* update default node version to 20.18.2 ([1b68d3e](https://gitlab.com/rxap/gitlab-ci/commit/1b68d3eb65b748bcac3093a8f54a4e8cbca8c357))

## [6.2.5](https://gitlab.com/rxap/gitlab-ci/compare/6.3.0-edge.21...6.3.0-edge.22) (2025-01-22)

## [6.2.5](https://gitlab.com/rxap/gitlab-ci/compare/6.2.4...6.2.5) (2025-01-22)

## [6.2.5-nightly.8](https://gitlab.com/rxap/gitlab-ci/compare/6.2.4...6.2.5) (2025-01-17)

## [6.3.0-edge.21](https://gitlab.com/rxap/gitlab-ci/compare/6.2.4...6.2.5) (2025-01-16)


### Bug Fixes

* add missing version input ([a5ef69b](https://gitlab.com/rxap/gitlab-ci/commit/a5ef69b0908f0b461d11cef34d094bf0b4c6e72a))

## [6.2.5-nightly.8](https://gitlab.com/rxap/gitlab-ci/compare/6.2.5-nightly.7...6.2.5-nightly.8) (2025-01-17)

## [6.3.0-edge.21](https://gitlab.com/rxap/gitlab-ci/compare/6.2.5-nightly.7...6.2.5-nightly.8) (2025-01-16)


### Bug Fixes

* add missing version input ([a5ef69b](https://gitlab.com/rxap/gitlab-ci/commit/a5ef69b0908f0b461d11cef34d094bf0b4c6e72a))

## [6.3.0-edge.20](https://gitlab.com/rxap/gitlab-ci/compare/6.2.5-nightly.7...6.2.5-nightly.8) (2025-01-16)

## [6.3.0-edge.21](https://gitlab.com/rxap/gitlab-ci/compare/6.3.0-edge.20...6.3.0-edge.21) (2025-01-16)


### Bug Fixes

* add missing version input ([a5ef69b](https://gitlab.com/rxap/gitlab-ci/commit/a5ef69b0908f0b461d11cef34d094bf0b4c6e72a))

## [6.3.0-edge.20](https://gitlab.com/rxap/gitlab-ci/compare/6.3.0-edge.19...6.3.0-edge.20) (2025-01-16)


### Bug Fixes

* support gcp service account key file ([85ada5e](https://gitlab.com/rxap/gitlab-ci/commit/85ada5ede84e0e880d4656df97dba4e87d237248))

## [6.2.5-nightly.7](https://gitlab.com/rxap/gitlab-ci/compare/6.3.0-edge.19...6.3.0-edge.20) (2025-01-16)

## [6.2.5-nightly.7](https://gitlab.com/rxap/gitlab-ci/compare/6.2.5-nightly.6...6.2.5-nightly.7) (2025-01-16)

## [6.3.0-edge.19](https://gitlab.com/rxap/gitlab-ci/compare/6.2.5-nightly.6...6.2.5-nightly.7) (2025-01-15)


### Bug Fixes

* remove default tags ([7fdb1af](https://gitlab.com/rxap/gitlab-ci/commit/7fdb1afd0851a1a8314a8adbf71e3e05b66afa56))

## [6.3.0-edge.19](https://gitlab.com/rxap/gitlab-ci/compare/6.3.0-edge.18...6.3.0-edge.19) (2025-01-15)


### Bug Fixes

* remove default tags ([7fdb1af](https://gitlab.com/rxap/gitlab-ci/commit/7fdb1afd0851a1a8314a8adbf71e3e05b66afa56))

## [6.2.5-nightly.6](https://gitlab.com/rxap/gitlab-ci/compare/6.3.0-edge.18...6.3.0-edge.19) (2024-12-11)

## [6.2.5-nightly.6](https://gitlab.com/rxap/gitlab-ci/compare/6.2.5-nightly.5...6.2.5-nightly.6) (2024-12-11)

## [6.3.0-edge.18](https://gitlab.com/rxap/gitlab-ci/compare/6.2.5-nightly.5...6.2.5-nightly.6) (2024-12-10)


### Features

* add nx affected templates ([4f2eaad](https://gitlab.com/rxap/gitlab-ci/commit/4f2eaadae4f0da0550f209b266630d1196f8ea2a))

## [6.3.0-edge.18](https://gitlab.com/rxap/gitlab-ci/compare/6.3.0-edge.17...6.3.0-edge.18) (2024-12-10)


### Features

* add nx affected templates ([4f2eaad](https://gitlab.com/rxap/gitlab-ci/commit/4f2eaadae4f0da0550f209b266630d1196f8ea2a))

## [6.2.5-nightly.5](https://gitlab.com/rxap/gitlab-ci/compare/6.3.0-edge.17...6.3.0-edge.18) (2024-11-10)

## [6.2.5-nightly.5](https://gitlab.com/rxap/gitlab-ci/compare/6.2.5-nightly.4...6.2.5-nightly.5) (2024-11-10)

## [6.3.0-edge.17](https://gitlab.com/rxap/gitlab-ci/compare/6.2.5-nightly.4...6.2.5-nightly.5) (2024-11-07)


### Bug Fixes

* support custom services for e2e service jobs ([d84bcb3](https://gitlab.com/rxap/gitlab-ci/commit/d84bcb3f474e96fd523eaf3659df0217c79367e6))

## [6.3.0-edge.17](https://gitlab.com/rxap/gitlab-ci/compare/6.3.0-edge.16...6.3.0-edge.17) (2024-11-07)


### Bug Fixes

* support custom services for e2e service jobs ([d84bcb3](https://gitlab.com/rxap/gitlab-ci/commit/d84bcb3f474e96fd523eaf3659df0217c79367e6))

## [6.2.5-nightly.4](https://gitlab.com/rxap/gitlab-ci/compare/6.3.0-edge.16...6.3.0-edge.17) (2024-11-05)

## [6.2.5-nightly.4](https://gitlab.com/rxap/gitlab-ci/compare/6.2.5-nightly.3...6.2.5-nightly.4) (2024-11-05)

## [6.3.0-edge.16](https://gitlab.com/rxap/gitlab-ci/compare/6.2.5-nightly.3...6.2.5-nightly.4) (2024-11-04)


### Bug Fixes

* set to correct default ([cfbc335](https://gitlab.com/rxap/gitlab-ci/commit/cfbc3354952d9e1b65501e3ce9056e730d1b0b2c))

## [6.3.0-edge.15](https://gitlab.com/rxap/gitlab-ci/compare/6.2.5-nightly.3...6.2.5-nightly.4) (2024-11-04)

## [6.3.0-edge.16](https://gitlab.com/rxap/gitlab-ci/compare/6.3.0-edge.15...6.3.0-edge.16) (2024-11-04)


### Bug Fixes

* set to correct default ([cfbc335](https://gitlab.com/rxap/gitlab-ci/commit/cfbc3354952d9e1b65501e3ce9056e730d1b0b2c))

## [6.3.0-edge.15](https://gitlab.com/rxap/gitlab-ci/compare/6.3.0-edge.14...6.3.0-edge.15) (2024-11-04)


### Bug Fixes

* add missing input type ([516968c](https://gitlab.com/rxap/gitlab-ci/commit/516968cc17e7a25b57b7bc5f2e3d90c266ddb0b6))

## [6.3.0-edge.14](https://gitlab.com/rxap/gitlab-ci/compare/6.3.0-edge.13...6.3.0-edge.14) (2024-11-04)


### Features

* support custom startup retry count ([da270d1](https://gitlab.com/rxap/gitlab-ci/commit/da270d14b630d7de6a984c2216a949737fcb8439))

## [6.2.5-nightly.3](https://gitlab.com/rxap/gitlab-ci/compare/6.3.0-edge.13...6.3.0-edge.14) (2024-10-30)

## [6.2.5-nightly.3](https://gitlab.com/rxap/gitlab-ci/compare/6.2.5-nightly.2...6.2.5-nightly.3) (2024-10-30)

## [6.3.0-edge.13](https://gitlab.com/rxap/gitlab-ci/compare/6.2.5-nightly.2...6.2.5-nightly.3) (2024-10-29)


### Features

* support custom services for the startup job ([ee1e600](https://gitlab.com/rxap/gitlab-ci/commit/ee1e600fa1a0034de3c074752ef7b7daba760d4f))

## [6.3.0-edge.13](https://gitlab.com/rxap/gitlab-ci/compare/6.3.0-edge.12...6.3.0-edge.13) (2024-10-29)


### Features

* support custom services for the startup job ([ee1e600](https://gitlab.com/rxap/gitlab-ci/commit/ee1e600fa1a0034de3c074752ef7b7daba760d4f))

## [6.2.5-nightly.2](https://gitlab.com/rxap/gitlab-ci/compare/6.3.0-edge.12...6.3.0-edge.13) (2024-09-26)

## [6.2.5-nightly.2](https://gitlab.com/rxap/gitlab-ci/compare/6.2.5-nightly.1...6.2.5-nightly.2) (2024-09-26)

## [6.3.0-edge.12](https://gitlab.com/rxap/gitlab-ci/compare/6.2.5-nightly.1...6.2.5-nightly.2) (2024-09-25)


### Features

* support custom services for the run-many job ([d68c4e0](https://gitlab.com/rxap/gitlab-ci/commit/d68c4e0b8777fccf2a577771ab411cce0d9d3616))

## [6.3.0-edge.12](https://gitlab.com/rxap/gitlab-ci/compare/6.3.0-edge.11...6.3.0-edge.12) (2024-09-25)


### Features

* support custom services for the run-many job ([d68c4e0](https://gitlab.com/rxap/gitlab-ci/commit/d68c4e0b8777fccf2a577771ab411cce0d9d3616))

## [6.2.5-nightly.1](https://gitlab.com/rxap/gitlab-ci/compare/6.3.0-edge.11...6.3.0-edge.12) (2024-08-17)

## [6.2.5-nightly.1](https://gitlab.com/rxap/gitlab-ci/compare/6.2.5-nightly.0...6.2.5-nightly.1) (2024-08-17)

## [6.3.0-edge.11](https://gitlab.com/rxap/gitlab-ci/compare/6.2.5-nightly.0...6.2.5-nightly.1) (2024-08-16)


### Bug Fixes

* support custom system packages for yarn install ([ae22937](https://gitlab.com/rxap/gitlab-ci/commit/ae22937eb02dbf32e5d6736a7b2a72a47c01e6b7))

## [6.3.0-edge.11](https://gitlab.com/rxap/gitlab-ci/compare/6.3.0-edge.10...6.3.0-edge.11) (2024-08-16)


### Bug Fixes

* support custom system packages for yarn install ([ae22937](https://gitlab.com/rxap/gitlab-ci/commit/ae22937eb02dbf32e5d6736a7b2a72a47c01e6b7))

## [6.2.5-nightly.0](https://gitlab.com/rxap/gitlab-ci/compare/6.3.0-edge.10...6.3.0-edge.11) (2024-07-26)

## [6.2.5-nightly.0](https://gitlab.com/rxap/gitlab-ci/compare/6.2.4...6.2.5-nightly.0) (2024-07-26)

## [6.3.0-edge.10](https://gitlab.com/rxap/gitlab-ci/compare/6.2.4...6.2.5-nightly.0) (2024-07-25)


### Bug Fixes

* test for the compose commend ([26b180e](https://gitlab.com/rxap/gitlab-ci/commit/26b180e1cded410b1cd1d257a05d1a79ccaa2105))

## [6.3.0-edge.10](https://gitlab.com/rxap/gitlab-ci/compare/6.3.0-edge.9...6.3.0-edge.10) (2024-07-25)


### Bug Fixes

* test for the compose commend ([26b180e](https://gitlab.com/rxap/gitlab-ci/commit/26b180e1cded410b1cd1d257a05d1a79ccaa2105))

## [6.2.4](https://gitlab.com/rxap/gitlab-ci/compare/6.3.0-edge.9...6.3.0-edge.10) (2024-07-25)

## [6.2.4](https://gitlab.com/rxap/gitlab-ci/compare/6.2.3...6.2.4) (2024-07-25)

## [6.3.0-edge.9](https://gitlab.com/rxap/gitlab-ci/compare/6.2.3...6.2.4) (2024-07-25)


### Features

* **helm:** support custom helm command flags ([b3577ac](https://gitlab.com/rxap/gitlab-ci/commit/b3577acc7a6206b0705631cf727165a67e67d033))

## [6.2.4-nightly.0](https://gitlab.com/rxap/gitlab-ci/compare/6.2.3...6.2.4) (2024-07-23)

## [6.3.0-edge.9](https://gitlab.com/rxap/gitlab-ci/compare/6.3.0-edge.8...6.3.0-edge.9) (2024-07-25)


### Features

* **helm:** support custom helm command flags ([b3577ac](https://gitlab.com/rxap/gitlab-ci/commit/b3577acc7a6206b0705631cf727165a67e67d033))

## [6.2.4-nightly.0](https://gitlab.com/rxap/gitlab-ci/compare/6.3.0-edge.8...6.3.0-edge.9) (2024-07-23)

## [6.2.4-nightly.0](https://gitlab.com/rxap/gitlab-ci/compare/6.2.3...6.2.4-nightly.0) (2024-07-23)

## [6.3.0-edge.8](https://gitlab.com/rxap/gitlab-ci/compare/6.2.3...6.2.4-nightly.0) (2024-07-23)


### Features

* **release-it:** support verbosity ([768e43b](https://gitlab.com/rxap/gitlab-ci/commit/768e43b1d4ad1caf7fe38d82e385226cd57b672b))


### Bug Fixes

* install yq from source ([0fb9d3d](https://gitlab.com/rxap/gitlab-ci/commit/0fb9d3d43bdc11e02ef15087beafd1ecdd4fcc5e))

## [6.3.0-edge.8](https://gitlab.com/rxap/gitlab-ci/compare/6.3.0-edge.7...6.3.0-edge.8) (2024-07-23)


### Features

* **release-it:** support verbosity ([768e43b](https://gitlab.com/rxap/gitlab-ci/commit/768e43b1d4ad1caf7fe38d82e385226cd57b672b))


### Bug Fixes

* install yq from source ([0fb9d3d](https://gitlab.com/rxap/gitlab-ci/commit/0fb9d3d43bdc11e02ef15087beafd1ecdd4fcc5e))
* **release-it:** simplify debug ([c74f301](https://gitlab.com/rxap/gitlab-ci/commit/c74f30115c12e328d94df646b0cadf669385af13))

## [6.3.0-edge.7](https://gitlab.com/rxap/gitlab-ci/compare/6.3.0-edge.6...6.3.0-edge.7) (2024-07-22)


### Bug Fixes

* **coverage-report:** exit if no coverage files found ([0683b6b](https://gitlab.com/rxap/gitlab-ci/commit/0683b6b77bde233c91710ba55da56c43da729d43))

## [6.2.3](https://gitlab.com/rxap/gitlab-ci/compare/6.3.0-edge.6...6.3.0-edge.7) (2024-07-22)

## [6.2.3](https://gitlab.com/rxap/gitlab-ci/compare/6.2.2...6.2.3) (2024-07-22)

## [6.3.0-edge.6](https://gitlab.com/rxap/gitlab-ci/compare/6.2.2...6.2.3) (2024-07-22)


### Bug Fixes

* **helm:** ensure dependencies are updated recursively ([6583bb5](https://gitlab.com/rxap/gitlab-ci/commit/6583bb56e6a2f618e3ca50eceb38e438d05250c7))

## [6.2.3-nightly.1](https://gitlab.com/rxap/gitlab-ci/compare/6.2.2...6.2.3) (2024-07-20)

## [6.3.0-edge.6](https://gitlab.com/rxap/gitlab-ci/compare/6.3.0-edge.5...6.3.0-edge.6) (2024-07-22)


### Bug Fixes

* **helm:** ensure dependencies are updated recursively ([6583bb5](https://gitlab.com/rxap/gitlab-ci/commit/6583bb56e6a2f618e3ca50eceb38e438d05250c7))

## [6.2.3-nightly.1](https://gitlab.com/rxap/gitlab-ci/compare/6.3.0-edge.5...6.3.0-edge.6) (2024-07-20)

## [6.2.3-nightly.1](https://gitlab.com/rxap/gitlab-ci/compare/6.2.3-nightly.0...6.2.3-nightly.1) (2024-07-20)

## [6.3.0-edge.5](https://gitlab.com/rxap/gitlab-ci/compare/6.2.3-nightly.0...6.2.3-nightly.1) (2024-07-19)


### Bug Fixes

* **nx:** add NX_VERBOSE_LOGGING variable option ([ebeeec6](https://gitlab.com/rxap/gitlab-ci/commit/ebeeec66895596571c2e24cd31e0c09e407f926e))

## [6.3.0-edge.5](https://gitlab.com/rxap/gitlab-ci/compare/6.3.0-edge.4...6.3.0-edge.5) (2024-07-19)


### Bug Fixes

* **nx:** add NX_VERBOSE_LOGGING variable option ([ebeeec6](https://gitlab.com/rxap/gitlab-ci/commit/ebeeec66895596571c2e24cd31e0c09e407f926e))

## [6.2.3-nightly.0](https://gitlab.com/rxap/gitlab-ci/compare/6.3.0-edge.4...6.3.0-edge.5) (2024-07-19)

## [6.2.3-nightly.0](https://gitlab.com/rxap/gitlab-ci/compare/6.2.2...6.2.3-nightly.0) (2024-07-19)

## [6.3.0-edge.4](https://gitlab.com/rxap/gitlab-ci/compare/6.2.2...6.2.3-nightly.0) (2024-07-18)


### Bug Fixes

* **helm:** trigger auto release for subchart and lock file changes ([976cb17](https://gitlab.com/rxap/gitlab-ci/commit/976cb1737913b0cac947fe20272bd4628cc631ae))

## [6.3.0-edge.3](https://gitlab.com/rxap/gitlab-ci/compare/6.2.2...6.2.3-nightly.0) (2024-07-18)

## [6.3.0-edge.4](https://gitlab.com/rxap/gitlab-ci/compare/6.3.0-edge.3...6.3.0-edge.4) (2024-07-18)


### Bug Fixes

* **helm:** trigger auto release for subchart and lock file changes ([976cb17](https://gitlab.com/rxap/gitlab-ci/commit/976cb1737913b0cac947fe20272bd4628cc631ae))

## [6.3.0-edge.3](https://gitlab.com/rxap/gitlab-ci/compare/6.3.0-edge.2...6.3.0-edge.3) (2024-07-18)


### Bug Fixes

* **startup:** handle unknown server pid ([24750ac](https://gitlab.com/rxap/gitlab-ci/commit/24750aced6551fc245d03042d143ff1b5d7b5610))

## [6.2.2](https://gitlab.com/rxap/gitlab-ci/compare/6.3.0-edge.2...6.3.0-edge.3) (2024-07-18)

## [6.2.2](https://gitlab.com/rxap/gitlab-ci/compare/6.2.1...6.2.2) (2024-07-18)

## [6.3.0-edge.2](https://gitlab.com/rxap/gitlab-ci/compare/6.2.1...6.2.2) (2024-07-18)


### Bug Fixes

* **helm:** use the update subcommand to ensure the repos are pulled ([55b81f4](https://gitlab.com/rxap/gitlab-ci/commit/55b81f411614e4288825ec4dc67bb76412e3a012))

## [6.3.0-edge.1](https://gitlab.com/rxap/gitlab-ci/compare/6.2.1...6.2.2) (2024-07-18)

## [6.3.0-edge.2](https://gitlab.com/rxap/gitlab-ci/compare/6.3.0-edge.1...6.3.0-edge.2) (2024-07-18)


### Bug Fixes

* **helm:** use the update subcommand to ensure the repos are pulled ([55b81f4](https://gitlab.com/rxap/gitlab-ci/commit/55b81f411614e4288825ec4dc67bb76412e3a012))

## [6.3.0-edge.1](https://gitlab.com/rxap/gitlab-ci/compare/6.3.0-edge.0...6.3.0-edge.1) (2024-07-18)


### Bug Fixes

* **helm:** run dependency build before other commands ([aac1281](https://gitlab.com/rxap/gitlab-ci/commit/aac1281fff6a4a080f80a7d8af70d535b852a96c))

## [6.2.2-nightly.0](https://gitlab.com/rxap/gitlab-ci/compare/6.3.0-edge.0...6.3.0-edge.1) (2024-07-17)

## [6.2.2-nightly.0](https://gitlab.com/rxap/gitlab-ci/compare/6.2.1...6.2.2-nightly.0) (2024-07-17)

## [6.3.0-edge.0](https://gitlab.com/rxap/gitlab-ci/compare/6.2.1...6.2.2-nightly.0) (2024-07-16)


### Features

* update all Chart.yaml files in the repository ([689c157](https://gitlab.com/rxap/gitlab-ci/commit/689c15753385844160621f98d3023348618a683a))

## [6.3.0-edge.0](https://gitlab.com/rxap/gitlab-ci/compare/6.2.1...6.3.0-edge.0) (2024-07-16)


### Features

* update all Chart.yaml files in the repository ([689c157](https://gitlab.com/rxap/gitlab-ci/commit/689c15753385844160621f98d3023348618a683a))

## [6.2.1](https://gitlab.com/rxap/gitlab-ci/compare/6.2.0...6.2.1) (2024-06-28)

## [6.2.1-edge.1](https://gitlab.com/rxap/gitlab-ci/compare/6.2.0...6.2.1) (2024-06-28)

## [6.2.1-edge.1](https://gitlab.com/rxap/gitlab-ci/compare/6.2.1-edge.0...6.2.1-edge.1) (2024-06-28)

## [6.2.1-edge.0](https://gitlab.com/rxap/gitlab-ci/compare/6.2.0...6.2.1-edge.0) (2024-06-28)


### Bug Fixes

* install required packages ([6c1ad59](https://gitlab.com/rxap/gitlab-ci/commit/6c1ad59bdef15d569ff2dbab79bfa8edb9160862))
* use semver sort ([c698e3f](https://gitlab.com/rxap/gitlab-ci/commit/c698e3f610917f2d64e3494a1558aaa62b59d9ec))

## [6.2.0](https://gitlab.com/rxap/gitlab-ci/compare/6.1.13...6.2.0) (2024-06-02)


### Features

* expose service startup logs to jpb log ([3038bde](https://gitlab.com/rxap/gitlab-ci/commit/3038bdec409ad4941bd682258384317298fa6710))


### Bug Fixes

* ensure the custom port is used ([013e5f9](https://gitlab.com/rxap/gitlab-ci/commit/013e5f99761d0c7b63249f7302093c3372df647d))

## [6.1.13](https://gitlab.com/rxap/gitlab-ci/compare/6.1.12...6.1.13) (2024-05-31)


### Bug Fixes

* file name ([762eaa2](https://gitlab.com/rxap/gitlab-ci/commit/762eaa23d5c7e5530e47646a922f414520f891b4))

## [6.1.13-edge.0](https://gitlab.com/rxap/gitlab-ci/compare/6.1.12...6.1.13) (2024-05-31)


### Bug Fixes

* add missing input to rxap compose component ([e248ddf](https://gitlab.com/rxap/gitlab-ci/commit/e248ddfded731eb8d6cbb18ffb675e4150452ea2))

## [6.1.13-edge.0](https://gitlab.com/rxap/gitlab-ci/compare/6.1.12...6.1.13-edge.0) (2024-05-31)


### Bug Fixes

* add missing input to rxap compose component ([e248ddf](https://gitlab.com/rxap/gitlab-ci/commit/e248ddfded731eb8d6cbb18ffb675e4150452ea2))

## [6.1.12](https://gitlab.com/rxap/gitlab-ci/compare/6.1.11...6.1.12) (2024-05-30)


### Bug Fixes

* detect os type ([60f096a](https://gitlab.com/rxap/gitlab-ci/commit/60f096a079fcd4d1bb00ee71249af511f2cd68b1))
* detect os type ([07bf078](https://gitlab.com/rxap/gitlab-ci/commit/07bf07854c16acefeba9254db2e3fc36491caa03))
* remove comments ([3615a43](https://gitlab.com/rxap/gitlab-ci/commit/3615a439e1819e4f0bf85038ffe0f9b7df54f85c))

## [6.1.11](https://gitlab.com/rxap/gitlab-ci/compare/6.1.10...6.1.11) (2024-05-30)


### Bug Fixes

* only skip if open merge and branch pipeline ([1f3ca63](https://gitlab.com/rxap/gitlab-ci/commit/1f3ca63c5fb4e0dc1d63d24291d991ec16fe3ad2))

## [6.1.10](https://gitlab.com/rxap/gitlab-ci/compare/6.1.9...6.1.10) (2024-05-30)


### Bug Fixes

* only run migrations if migrations.json exists ([11866b1](https://gitlab.com/rxap/gitlab-ci/commit/11866b12b9dfe020945bcbe77ebda66c048b0397))

## [6.1.10-edge.7](https://gitlab.com/rxap/gitlab-ci/compare/6.1.10-edge.6...6.1.10-edge.7) (2024-05-30)


### Bug Fixes

* correctly normalize the package name ([53051aa](https://gitlab.com/rxap/gitlab-ci/commit/53051aa351fb6da404bfde88741873e1760535d9))

## [6.1.10-edge.6](https://gitlab.com/rxap/gitlab-ci/compare/6.1.10-edge.5...6.1.10-edge.6) (2024-05-30)


### Bug Fixes

* only test components amount if changed ([5ce29ad](https://gitlab.com/rxap/gitlab-ci/commit/5ce29ad24e363efbd50e1ece4540cfa09bcfc2df))

## [6.1.10-edge.5](https://gitlab.com/rxap/gitlab-ci/compare/6.1.10-edge.4...6.1.10-edge.5) (2024-05-30)


### Bug Fixes

* ensure only valid environment names are defined ([9e3bf25](https://gitlab.com/rxap/gitlab-ci/commit/9e3bf252e95980500a7cd09b0185f454e078c47f))

## [6.1.10-edge.4](https://gitlab.com/rxap/gitlab-ci/compare/6.1.10-edge.3...6.1.10-edge.4) (2024-05-30)


### Bug Fixes

* ensure only valid environment names are defined ([80c4124](https://gitlab.com/rxap/gitlab-ci/commit/80c41246e961c779fa6edfd77d98b0aafeabcbf1))

## [6.1.10-edge.3](https://gitlab.com/rxap/gitlab-ci/compare/6.1.10-edge.2...6.1.10-edge.3) (2024-05-30)


### Bug Fixes

* support scoped package names in migrate branches ([f1b0c9e](https://gitlab.com/rxap/gitlab-ci/commit/f1b0c9ed2a6f9fe67b49f9ce4bb0cdbe5b269817))

## [6.1.10-edge.2](https://gitlab.com/rxap/gitlab-ci/compare/6.1.10-edge.1...6.1.10-edge.2) (2024-05-30)


### Bug Fixes

* simplify env name definition ([bbf44d6](https://gitlab.com/rxap/gitlab-ci/commit/bbf44d63900752033e88f859ad9767ec3d567e5f))

## [6.1.10-edge.1](https://gitlab.com/rxap/gitlab-ci/compare/6.1.10-edge.0...6.1.10-edge.1) (2024-05-30)


### Bug Fixes

* support dynamic environment names ([d2778c2](https://gitlab.com/rxap/gitlab-ci/commit/d2778c2c3db8c11da1541bd9742631a5d3d305c1))

## [6.1.10-edge.0](https://gitlab.com/rxap/gitlab-ci/compare/6.1.9...6.1.10-edge.0) (2024-05-30)


### Bug Fixes

* ensure the repo has at most 30 components ([9c0293c](https://gitlab.com/rxap/gitlab-ci/commit/9c0293c3f4d9de58ac2eae48f8dfebaf1b3a00e2))

## [6.1.9](https://gitlab.com/rxap/gitlab-ci/compare/6.1.8...6.1.9) (2024-05-30)


### Bug Fixes

* prevent execution loop ([26890c3](https://gitlab.com/rxap/gitlab-ci/commit/26890c337fe73647b01ebf0e2fadc3e4fee78f49))

## [6.1.8](https://gitlab.com/rxap/gitlab-ci/compare/6.1.7...6.1.8) (2024-05-29)

## [6.2.0-edge.19](https://gitlab.com/rxap/gitlab-ci/compare/6.1.7...6.1.8) (2024-05-29)

## [6.2.0-edge.19](https://gitlab.com/rxap/gitlab-ci/compare/6.2.0-edge.18...6.2.0-edge.19) (2024-05-29)


### Features

* support custom tags via inputs ([6e3b4b6](https://gitlab.com/rxap/gitlab-ci/commit/6e3b4b6a122d2bea309d245a9d54e93d71dc70e1))

## [6.2.0-edge.18](https://gitlab.com/rxap/gitlab-ci/compare/6.2.0-edge.17...6.2.0-edge.18) (2024-05-29)

## [6.1.8-nightly.1](https://gitlab.com/rxap/gitlab-ci/compare/6.1.8-nightly.0...6.1.8-nightly.1) (2024-05-29)

## [6.2.0-edge.17](https://gitlab.com/rxap/gitlab-ci/compare/6.1.8-nightly.0...6.1.8-nightly.1) (2024-05-28)

## [6.2.0-edge.17](https://gitlab.com/rxap/gitlab-ci/compare/6.2.0-edge.16...6.2.0-edge.17) (2024-05-28)


### Bug Fixes

* **nx-migrate:** use correct label splitter ([101fbfb](https://gitlab.com/rxap/gitlab-ci/commit/101fbfb490db8d8eea496b83c3fb8d13c80cbe15))

## [6.2.0-edge.16](https://gitlab.com/rxap/gitlab-ci/compare/6.2.0-edge.15...6.2.0-edge.16) (2024-05-28)


### Bug Fixes

* **nx-workspace:** skip if branch pipeline ([408c34e](https://gitlab.com/rxap/gitlab-ci/commit/408c34eae36ed6a147407b2852dc59b848971920))

## [6.2.0-edge.15](https://gitlab.com/rxap/gitlab-ci/compare/6.2.0-edge.14...6.2.0-edge.15) (2024-05-28)


### Bug Fixes

* ensure yarn.lock is up-to-date ([305d70d](https://gitlab.com/rxap/gitlab-ci/commit/305d70d1918da4f887a0558827e26151f1109239))

## [6.2.0-edge.14](https://gitlab.com/rxap/gitlab-ci/compare/6.2.0-edge.13...6.2.0-edge.14) (2024-05-28)


### Bug Fixes

* **nx-migrate:** support custom post migration script ([c5805a4](https://gitlab.com/rxap/gitlab-ci/commit/c5805a415686d176c33c598670eed54b0649c6ae))

## [6.2.0-edge.13](https://gitlab.com/rxap/gitlab-ci/compare/6.2.0-edge.12...6.2.0-edge.13) (2024-05-28)


### Bug Fixes

* **rxap-compose:** support custom compose command and json schematic files ([564768e](https://gitlab.com/rxap/gitlab-ci/commit/564768e92152622a5e88b410c04f5260df34183a))

## [6.2.0-edge.12](https://gitlab.com/rxap/gitlab-ci/compare/6.2.0-edge.11...6.2.0-edge.12) (2024-05-28)


### Bug Fixes

* **npm-cache:** build cache for merge request pipelines ([eba4a37](https://gitlab.com/rxap/gitlab-ci/commit/eba4a37523081ffef2d1878175f4787b6b2b257d))

## [6.2.0-edge.11](https://gitlab.com/rxap/gitlab-ci/compare/6.2.0-edge.10...6.2.0-edge.11) (2024-05-28)


### Bug Fixes

* use correct function name ([86ed26e](https://gitlab.com/rxap/gitlab-ci/commit/86ed26e12682277afef1790dc0366956e59e8384))

## [6.2.0-edge.10](https://gitlab.com/rxap/gitlab-ci/compare/6.2.0-edge.9...6.2.0-edge.10) (2024-05-28)


### Bug Fixes

* **npm-cache:** add missing quotes ([5430ca8](https://gitlab.com/rxap/gitlab-ci/commit/5430ca8dbe2ee9c0ac3fa7c8fd7f9232648882fc))

## [6.2.0-edge.9](https://gitlab.com/rxap/gitlab-ci/compare/6.2.0-edge.8...6.2.0-edge.9) (2024-05-28)

## [6.2.0-edge.8](https://gitlab.com/rxap/gitlab-ci/compare/6.2.0-edge.7...6.2.0-edge.8) (2024-05-28)


### Bug Fixes

* **rxap-compose:** only use the changes rule if the pipeline source push ([e6f42ba](https://gitlab.com/rxap/gitlab-ci/commit/e6f42ba19bf1e25a60a918b275f09e8abf0bf4ad))

## [6.2.0-edge.7](https://gitlab.com/rxap/gitlab-ci/compare/6.2.0-edge.6...6.2.0-edge.7) (2024-05-28)


### Bug Fixes

* **nx-migrate:** only run the trigger if the NX_MIGRATE_PACKAGE env is not false ([e872518](https://gitlab.com/rxap/gitlab-ci/commit/e87251812af641a9228559fbccdf67bdffb8a5ea))

## [6.2.0-edge.6](https://gitlab.com/rxap/gitlab-ci/compare/6.2.0-edge.5...6.2.0-edge.6) (2024-05-28)


### Bug Fixes

* remove empty job ([b548400](https://gitlab.com/rxap/gitlab-ci/commit/b548400031f8adb7cd7f231dc9b6246b6ad211bb))

## [6.2.0-edge.5](https://gitlab.com/rxap/gitlab-ci/compare/6.2.0-edge.4...6.2.0-edge.5) (2024-05-28)


### Features

* support nx-migrate and rxap-compose dynamic triggers ([a887087](https://gitlab.com/rxap/gitlab-ci/commit/a887087497bc2ac7a68c5ea7a3dae413bef60963))

## [6.2.0-edge.4](https://gitlab.com/rxap/gitlab-ci/compare/6.2.0-edge.3...6.2.0-edge.4) (2024-05-28)


### Bug Fixes

* ensure the run jobs are added to merge request pipelines ([f121180](https://gitlab.com/rxap/gitlab-ci/commit/f121180534a8154b128a2d2787a58397a973b736))

## [6.2.0-edge.3](https://gitlab.com/rxap/gitlab-ci/compare/6.2.0-edge.2...6.2.0-edge.3) (2024-05-28)


### Bug Fixes

* **nx:** add migration failed and successful labels ([cb05d67](https://gitlab.com/rxap/gitlab-ci/commit/cb05d670c6b8849c81c34086c7e35881f164cdea))

## [6.2.0-edge.2](https://gitlab.com/rxap/gitlab-ci/compare/6.2.0-edge.1...6.2.0-edge.2) (2024-05-28)


### Bug Fixes

* **git-ssh:** correctly initialize the variable ([7c9ea35](https://gitlab.com/rxap/gitlab-ci/commit/7c9ea35cebe8af2807849ff273c3e6ff03c80b16))

## [6.1.8-nightly.0](https://gitlab.com/rxap/gitlab-ci/compare/6.1.7...6.1.8-nightly.0) (2024-05-28)

## [6.2.0-edge.1](https://gitlab.com/rxap/gitlab-ci/compare/6.1.7...6.1.8-nightly.0) (2024-05-27)

## [6.2.0-edge.1](https://gitlab.com/rxap/gitlab-ci/compare/6.2.0-edge.0...6.2.0-edge.1) (2024-05-27)


### Features

* **git-ssh:** add utility functions ([f1982c3](https://gitlab.com/rxap/gitlab-ci/commit/f1982c38327ef34a80dee864a07bd826199cafe8))

## [6.2.0-edge.0](https://gitlab.com/rxap/gitlab-ci/compare/6.1.7...6.2.0-edge.0) (2024-05-27)


### Features

* **nx-migrate:** add the nx migrate job ([3124aea](https://gitlab.com/rxap/gitlab-ci/commit/3124aea158199f70945a82db8f628ee36463ffdf))

## [6.1.7](https://gitlab.com/rxap/gitlab-ci/compare/6.1.6...6.1.7) (2024-05-27)

## [6.1.7-nightly.2](https://gitlab.com/rxap/gitlab-ci/compare/6.1.6...6.1.7) (2024-05-23)

## [6.1.7-nightly.2](https://gitlab.com/rxap/gitlab-ci/compare/6.1.7-nightly.1...6.1.7-nightly.2) (2024-05-23)

## [6.1.7-edge.7](https://gitlab.com/rxap/gitlab-ci/compare/6.1.7-nightly.1...6.1.7-nightly.2) (2024-05-22)

## [6.1.7-edge.7](https://gitlab.com/rxap/gitlab-ci/compare/6.1.7-edge.6...6.1.7-edge.7) (2024-05-22)


### Bug Fixes

* add npm cache to nx before_script ([5fed7d3](https://gitlab.com/rxap/gitlab-ci/commit/5fed7d32cff418f772ef32cd660be837eaad7f1b))

## [6.1.7-nightly.1](https://gitlab.com/rxap/gitlab-ci/compare/6.1.7-nightly.0...6.1.7-nightly.1) (2024-05-21)

## [6.1.7-edge.6](https://gitlab.com/rxap/gitlab-ci/compare/6.1.7-nightly.0...6.1.7-nightly.1) (2024-05-20)

## [6.1.7-edge.6](https://gitlab.com/rxap/gitlab-ci/compare/6.1.7-edge.5...6.1.7-edge.6) (2024-05-20)

## [6.1.7-edge.5](https://gitlab.com/rxap/gitlab-ci/compare/6.1.7-edge.4...6.1.7-edge.5) (2024-05-20)

## [6.1.7-nightly.0](https://gitlab.com/rxap/gitlab-ci/compare/6.1.6...6.1.7-nightly.0) (2024-05-20)

## [6.1.7-edge.4](https://gitlab.com/rxap/gitlab-ci/compare/6.1.6...6.1.7-nightly.0) (2024-05-18)

## [6.1.7-edge.4](https://gitlab.com/rxap/gitlab-ci/compare/6.1.7-edge.3...6.1.7-edge.4) (2024-05-18)

## [6.1.7-edge.3](https://gitlab.com/rxap/gitlab-ci/compare/6.1.7-edge.2...6.1.7-edge.3) (2024-05-18)


### Bug Fixes

* use the correct value for when ([436da07](https://gitlab.com/rxap/gitlab-ci/commit/436da0773e4cb8c13d54fd6056cfb8ff17938fc3))

## [6.1.7-edge.2](https://gitlab.com/rxap/gitlab-ci/compare/6.1.7-edge.1...6.1.7-edge.2) (2024-05-18)

## [6.1.7-edge.1](https://gitlab.com/rxap/gitlab-ci/compare/6.1.7-edge.0...6.1.7-edge.1) (2024-05-18)

## [6.1.7-edge.0](https://gitlab.com/rxap/gitlab-ci/compare/6.1.6...6.1.7-edge.0) (2024-05-18)

## [6.1.6](https://gitlab.com/rxap/gitlab-ci/compare/6.1.5...6.1.6) (2024-05-18)

## [6.1.6-edge.4](https://gitlab.com/rxap/gitlab-ci/compare/6.1.5...6.1.6) (2024-05-18)

## [6.1.6-edge.4](https://gitlab.com/rxap/gitlab-ci/compare/6.1.6-edge.3...6.1.6-edge.4) (2024-05-18)

## [6.1.6-edge.3](https://gitlab.com/rxap/gitlab-ci/compare/6.1.6-edge.2...6.1.6-edge.3) (2024-05-18)

## [6.1.6-edge.2](https://gitlab.com/rxap/gitlab-ci/compare/6.1.6-edge.1...6.1.6-edge.2) (2024-05-18)

## [6.1.6-edge.1](https://gitlab.com/rxap/gitlab-ci/compare/6.1.6-edge.0...6.1.6-edge.1) (2024-05-18)

## [6.1.6-edge.0](https://gitlab.com/rxap/gitlab-ci/compare/6.1.5...6.1.6-edge.0) (2024-05-18)

## [6.1.5](https://gitlab.com/rxap/gitlab-ci/compare/6.1.4...6.1.5) (2024-05-17)

## [6.1.5-edge.2](https://gitlab.com/rxap/gitlab-ci/compare/6.1.4...6.1.5) (2024-05-17)

## [6.1.5-edge.2](https://gitlab.com/rxap/gitlab-ci/compare/6.1.5-edge.1...6.1.5-edge.2) (2024-05-17)


### Bug Fixes

* nx workspace pipeline construction ([dc184f8](https://gitlab.com/rxap/gitlab-ci/commit/dc184f8f54550b8f046aa79c5022d4e626a63ae8))

## [6.1.5-edge.1](https://gitlab.com/rxap/gitlab-ci/compare/6.1.5-edge.0...6.1.5-edge.1) (2024-05-17)

## [6.1.5-edge.0](https://gitlab.com/rxap/gitlab-ci/compare/6.1.4...6.1.5-edge.0) (2024-05-17)


### Bug Fixes

* **run-many-dte:** disable parallel input usage ([2306147](https://gitlab.com/rxap/gitlab-ci/commit/2306147a675952eb8d27178d7c9799b9dbaabc63))

## [6.1.4](https://gitlab.com/rxap/gitlab-ci/compare/6.1.3...6.1.4) (2024-05-17)

## [6.1.4-edge.1](https://gitlab.com/rxap/gitlab-ci/compare/6.1.3...6.1.4) (2024-05-17)

## [6.1.4-edge.1](https://gitlab.com/rxap/gitlab-ci/compare/6.1.4-edge.0...6.1.4-edge.1) (2024-05-17)


### Bug Fixes

* change the datatype of the parallel input to number ([a28e9be](https://gitlab.com/rxap/gitlab-ci/commit/a28e9be344059fac5a91ac7e48fc93bfc556d08a))

## [6.1.3](https://gitlab.com/rxap/gitlab-ci/compare/6.1.2...6.1.3) (2024-05-17)


### Bug Fixes

* **publish-helm-chart:** cleanup the helm repo before adding a pre-release repo ([105d493](https://gitlab.com/rxap/gitlab-ci/commit/105d4935f9869a8bc08a4294ef8be38845dc285e))

## [6.1.2](https://gitlab.com/rxap/gitlab-ci/compare/6.1.1...6.1.2) (2024-05-17)

## [6.1.2-edge.2](https://gitlab.com/rxap/gitlab-ci/compare/6.1.1...6.1.2) (2024-05-17)

## [6.1.2-edge.2](https://gitlab.com/rxap/gitlab-ci/compare/6.1.2-edge.1...6.1.2-edge.2) (2024-05-17)


### Bug Fixes

* **docker-release-it:** if target is release and current is pre-release create new release ([0c2d8e3](https://gitlab.com/rxap/gitlab-ci/commit/0c2d8e31e3f0b4666682d0d850cce74d96444ef7))

## [6.1.2-edge.1](https://gitlab.com/rxap/gitlab-ci/compare/6.1.2-edge.0...6.1.2-edge.1) (2024-05-17)


### Bug Fixes

* **docker-release-it:** add missing function parameter ([1ea5af9](https://gitlab.com/rxap/gitlab-ci/commit/1ea5af9e3dc0d6473388e4fdb726d8c9f3dca562))

## [6.1.2-edge.0](https://gitlab.com/rxap/gitlab-ci/compare/6.1.1...6.1.2-edge.0) (2024-05-17)


### Bug Fixes

* **push-helm-chart:** add missing entrypoint overwrite ([5b44964](https://gitlab.com/rxap/gitlab-ci/commit/5b4496462d10ee72ee582a353cdfd9266ddafdbf))

## [6.1.1](https://gitlab.com/rxap/gitlab-ci/compare/6.1.0...6.1.1) (2024-05-17)


### Bug Fixes

* change file type from yaml to yml ([b8744a7](https://gitlab.com/rxap/gitlab-ci/commit/b8744a7875041e66c0fa554b405aad2b9b9fbd8f))

## [6.1.0](https://gitlab.com/rxap/gitlab-ci/compare/6.0.0...6.1.0) (2024-05-17)


### Features

* add the helm chart component ([ae45670](https://gitlab.com/rxap/gitlab-ci/commit/ae45670cc7f4fb154afeb075696c5604de507175))

## [6.0.0](https://gitlab.com/rxap/gitlab-ci/compare/5.3.7...6.0.0) (2024-05-17)

## [5.3.7](https://gitlab.com/rxap/gitlab-ci/compare/v5.3.6...v5.3.7) (2023-03-01)


### Bug Fixes

* **semantic-release:** update to latest pacakges ([084a1fd](https://gitlab.com/rxap/gitlab-ci/commit/084a1fdcf2ad27d79347fb0962c9297449703b6f))

## [5.3.6](https://gitlab.com/rxap/gitlab-ci/compare/v5.3.5...v5.3.6) (2022-06-20)


### Bug Fixes

* remove default target options ([836373f](https://gitlab.com/rxap/gitlab-ci/commit/836373f5ad21177216b3b8e8e8b20c6d020bf663))

## [5.3.5](https://gitlab.com/rxap/gitlab-ci/compare/v5.3.4...v5.3.5) (2022-06-20)


### Bug Fixes

* use global env ([68469bd](https://gitlab.com/rxap/gitlab-ci/commit/68469bdb37e0d19a0d78a0e43078d9ff4cc88a9c))

## [5.3.4](https://gitlab.com/rxap/gitlab-ci/compare/v5.3.3...v5.3.4) (2022-05-05)


### Bug Fixes

* bash scripts ([eb7a9ee](https://gitlab.com/rxap/gitlab-ci/commit/eb7a9eea6a2c29f4441b5ce7215c11caa7317df1))

## [5.3.3](https://gitlab.com/rxap/gitlab-ci/compare/v5.3.2...v5.3.3) (2022-05-05)


### Bug Fixes

* check if start of line ([d2b9e0f](https://gitlab.com/rxap/gitlab-ci/commit/d2b9e0ff684accb5e6e0768ac53f5d64400bd649))

## [5.3.2](https://gitlab.com/rxap/gitlab-ci/compare/v5.3.1...v5.3.2) (2022-05-05)


### Bug Fixes

* target name extraction ([c8a5d22](https://gitlab.com/rxap/gitlab-ci/commit/c8a5d222f1488a9095f9204c3ecfd2b7756bf149))

## [5.3.1](https://gitlab.com/rxap/gitlab-ci/compare/v5.3.0...v5.3.1) (2022-04-20)


### Bug Fixes

* only update firebase jjson if not target mode ([5335fc0](https://gitlab.com/rxap/gitlab-ci/commit/5335fc0bb51366d8e0c70481edd059eec6ecbf28))

# [5.3.0](https://gitlab.com/rxap/gitlab-ci/compare/v5.2.1...v5.3.0) (2022-04-20)


### Features

* support firebase target for hosting deployments ([68fbe42](https://gitlab.com/rxap/gitlab-ci/commit/68fbe42ee6a328bf4acdec3ae9cdbe3998a0dbb6))

## [5.2.1](https://gitlab.com/rxap/gitlab-ci/compare/v5.2.0...v5.2.1) (2022-03-29)


### Bug Fixes

* use custom implementation to prevent index.html overwriting ([b4d47d8](https://gitlab.com/rxap/gitlab-ci/commit/b4d47d87c68438100566c8e2dc9d31cef7ef7430))

# [5.2.0](https://gitlab.com/rxap/gitlab-ci/compare/v5.1.1...v5.2.0) (2022-03-28)


### Features

* add apindex docker image ([53fcc69](https://gitlab.com/rxap/gitlab-ci/commit/53fcc6964eeb8e26d63b61f486a6dc8636675ab2))

## [5.1.1](https://gitlab.com/rxap/gitlab-ci/compare/v5.1.0...v5.1.1) (2022-03-25)


### Bug Fixes

* use relative path to aaccess nx bin ([363fea6](https://gitlab.com/rxap/gitlab-ci/commit/363fea69ee2bc5b00ad3e2dc3ae69f809b94e4e9))

# [5.1.0](https://gitlab.com/rxap/gitlab-ci/compare/v5.0.5...v5.1.0) (2022-03-04)


### Features

* **templates-firebase:** rename .deploy to .firebase.deploy ([693e7fc](https://gitlab.com/rxap/gitlab-ci/commit/693e7fcf1af3f41f9795910a57be9b8ca8d31dc6))

## [5.0.5](https://gitlab.com/rxap/gitlab-ci/compare/v5.0.4...v5.0.5) (2022-03-04)


### Bug Fixes

* remove required parameter configuration ([d73c20e](https://gitlab.com/rxap/gitlab-ci/commit/d73c20ec4629faef572ec301ad1a7657ee21ac24))
