Collection of GitLab CI configs
===

# Components

## Docker

### GCP Artifact Registry

**Environment Variables**

 Name                         | Default          | Description                                    | Protected | Masked | Hidden 
------------------------------|------------------|------------------------------------------------|-----------|--------|--------
 GCP_ARTIFACT_REGISTRY        | gitlab           | Artifact Registry ID/Name                      | true      | false  | false  
 GCP_PROJECT                  |                  | Project ID                                     | true      | false  | false  
 GCP_REGION                   | europe-west1     | Resgion ID                                     | true      | false  | false  
 PUSH_TO_GCP                  | false            | Toogle to enable push to GCP Artifact Registry | true      | false  | false  
 GCP_SERVICE_ACCOUNT_KEY_FILE |                  | service account key file as json or base64     | true      | true   | true   
 GCP_IMAGE_NAME               | $CI_PROJECT_NAME | the base name of the imge                      | true      | false  | false  

**Setup Service Account**

```SH
GCP_PROJECT=...
GCP_SERVICE_ACCOUNT_NAME="gitlab-ci-artifact-registry"

gcloud iam service-accounts create ${GCP_SERVICE_ACCOUNT_NAME} \
  --description="Use in the gitlab ci to push images to the artifact registry" \
  --display-name="GitLab CI Artifact Registry"
  
gcloud projects add-iam-policy-binding ${GCP_PROJECT} \
  --member="serviceAccount:${GCP_SERVICE_ACCOUNT_NAME}@${GCP_PROJECT}.iam.gserviceaccount.com" \
  --role="roles/iam.serviceAccountUser"

gcloud projects add-iam-policy-binding ${GCP_PROJECT} \
  --member="serviceAccount:${GCP_SERVICE_ACCOUNT_NAME}@${GCP_PROJECT}.iam.gserviceaccount.com" \
  --role="roles/artifactregistry.writer"
  
gcloud iam service-accounts keys create ${GCP_SERVICE_ACCOUNT_NAME}-key.json \
    --iam-account=${GCP_SERVICE_ACCOUNT_NAME}@${GCP_PROJECT}.iam.gserviceaccount.com
 
cat ${GCP_SERVICE_ACCOUNT_NAME}-key.json | base64 -w 0
```

## gcloud

### Cloud Run

**Environment Variables**

 Name                                   | Default          | Description                                     | Protected | Masked | Hidden 
----------------------------------------|------------------|-------------------------------------------------|-----------|--------|--------
 GCP_CLOUD_RUN_SERVICE_ACCOUNT_KEY_FILE |                  | service account key file as base64 encoded json | true      | true   | true   
 GCP_ARTIFACT_REGISTRY                  | gitlab           | Artifact Registry ID/Name                       | true      | false  | false  
 GCP_PROJECT                            |                  | Project ID                                      | true      | false  | false  
 GCP_REGION                             | europe-west1     | Resgion ID                                      | true      | false  | false  
 GCP_IMAGE_NAME                         | $CI_PROJECT_NAME | the base name of the imge                       | true      | false  | false  

**Setup Service Account**

```SH
GCP_PROJECT=...
GCP_SERVICE_ACCOUNT_NAME="gitlab-ci-cloud-run"

gcloud iam service-accounts create ${GCP_SERVICE_ACCOUNT_NAME} \
  --description="Use in the gitlab ci to mange the deployment of cloud run servies" \
  --display-name="GitLab CI Cloud Run"
  
gcloud projects add-iam-policy-binding ${GCP_PROJECT} \
  --member="serviceAccount:${GCP_SERVICE_ACCOUNT_NAME}@${GCP_PROJECT}.iam.gserviceaccount.com" \
  --role="roles/iam.serviceAccountUser"

gcloud projects add-iam-policy-binding ${GCP_PROJECT} \
  --member="serviceAccount:${GCP_SERVICE_ACCOUNT_NAME}@${GCP_PROJECT}.iam.gserviceaccount.com" \
  --role="roles/run.developer"

gcloud projects add-iam-policy-binding ${GCP_PROJECT} \
  --member="serviceAccount:${GCP_SERVICE_ACCOUNT_NAME}@${GCP_PROJECT}.iam.gserviceaccount.com" \
  --role="roles/artifactregistry.reader"
  
gcloud iam service-accounts keys create ${GCP_SERVICE_ACCOUNT_NAME}-key.json \
    --iam-account=${GCP_SERVICE_ACCOUNT_NAME}@${GCP_PROJECT}.iam.gserviceaccount.com
 
cat ${GCP_SERVICE_ACCOUNT_NAME}-key.json | base64 -w 0
```
