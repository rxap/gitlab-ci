#!/bin/bash

docker run \
--rm \
-e GITHUB_COM_TOKEN=$GITHUB_COM_TOKEN \
-e RENOVATE_TOKEN=$RENOVATE_TOKEN \
renovate/renovate:slim \
--platform gitlab \
--endpoint https://gitlab.com/api/v4/ \
--labels renovate \
--onboarding-config='{"extends":["gitlab>rxap/renovate-config"]}' \
--onboarding true \
--binary-source npm \
$(cat repositories.list | xargs) \
"$@"
