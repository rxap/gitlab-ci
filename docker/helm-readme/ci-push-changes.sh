#!/bin/sh

# exit on error
set -e

# exit if CI_COMMIT_BRANCH is not set
if [ -z "$CI_COMMIT_BRANCH" ]; then
  echo "CI_COMMIT_BRANCH is not set"
  exit 1
fi

current_remote=$(git config --get "branch.$CI_COMMIT_BRANCH.remote")

echo "Pushing the changes to the branch $CI_COMMIT_BRANCH in the remote $current_remote"
git push --no-verify "$current_remote" "$CI_COMMIT_BRANCH"

echo "Done"
