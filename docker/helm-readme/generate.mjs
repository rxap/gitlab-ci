#!/usr/bin/env node

import Handlebars from "handlebars";
import { existsSync, readFileSync, writeFileSync } from 'fs';

const README_TEMPLATE_FILE_PATH = "README.md.hbs";

function hasTemplate() {
  return existsSync(README_TEMPLATE_FILE_PATH);
}

function loadTemplate() {

  if (!hasTemplate()) {
    throw new Error(`Template file not found: ${README_TEMPLATE_FILE_PATH}`);
  }

  console.log(`Loading README template from ${README_TEMPLATE_FILE_PATH}`)
  const content = readFileSync(README_TEMPLATE_FILE_PATH, 'utf-8');

  return Handlebars.compile(content);

}

function writeReadme(content) {
  console.log("Writing README.md");
  writeFileSync("README.md", content);
}

function main() {

  const template = loadTemplate();
  const context = {
    env: process.env
  };
  console.log("Rendering README template")
  const content = template(context);
  writeReadme(content);

  console.log('Done!')

}

main();
