#!/usr/bin/env node

import { readFileSync, existsSync, writeFileSync } from 'fs';
import { relative } from 'path';
import { parseDocument } from 'yaml';
import { globSync } from 'glob';
import 'colors';

/**
 * Checks if the chart file exists.
 *
 * @param {string} chartFilePath - The path to the chart file.
 *
 * @return {boolean} true if the chart file exists, false otherwise.
 */
function hasChartFile(chartFilePath) {
  return existsSync(chartFilePath);
}

/**
 * Retrieves the chart from the chart file.
 *
 * @param {string} chartFilePath - The path to the chart file.
 *
 * @throws {Error} If the chart file is not found at the specified path.
 * @throws {Error} If the chart file fails to parse.
 *
 * @returns {Parsed<ParsedNode, true>} The parsed chart.
 */
function getChart(chartFilePath) {

  if (!hasChartFile(chartFilePath)) {
    throw new Error(`Chart file not found at '${process.cwd()}/${chartFilePath}'`);
  }

  const content = readFileSync(chartFilePath, 'utf8');

  try {
    return parseDocument(content);
  } catch (error) {
    throw new Error(`Failed to parse chart file: ${error.message}`);
  }

}

/**
 * Retrieves the app version from the chart file.
 *
 * @param {string} chartFilePath - The path to the chart file.
 *
 * @throws {Error} If the app version is not found in the chart file.
 *
 * @return {string} The app version value.
 */
function getChartAppVersion(chartFilePath) {
  const chart = getChart(chartFilePath);
  const version = chart.get('appVersion');

  if (!version) {
    throw new Error('App Version not found in chart file');
  }

  return version.value;
}

/**
 * Set the version number of the chart application.
 *
 * @param {string} version - The version number of the chart application.
 * @param {string} chartFilePath - The path to the chart file.
 *
 * @returns {void}
 */
function setChartAppVersion(version, chartFilePath) {

  const chart = getChart(chartFilePath);
  chart.set('appVersion', version);

  updateChartFile(chart);

}

/**
 * Update the chart file with the provided content.
 *
 * @param {Parsed<ParsedNode, true>} chart - The chart data to be updated.
 * @param {string} chartFilePath - The path to the chart file.
 *
 * @return {void}
 */
function updateChartFile(chart, chartFilePath) {
  const content = chart.toString();
  console.log(`Updating chart file: ${chartFilePath}`.grey)
  writeFileSync(chartFilePath, content);
}

/**
 * Guesses the repository type from the given URL.
 *
 * @param {string} url - The URL of the repository.
 * @return {string} - The guessed repository type, either 'github' or 'gitlab'.
 * @throws {Error} - If the repository type cannot be guessed from the URL.
 */
function guessRepoTypeFromUrl(url) {
  if (url.includes('github.com')) {
    return 'github';
  }
  if (url.includes('gitlab.com')) {
    return 'gitlab';
  }
  throw new Error(`Could not guess repo type from url: ${url}`);
}

/**
 * Get the latest version from the upstream repository.
 *
 * @param {string} repoType - The type of the repository.
 * @param {string} repoUrl - The URL of the repository.
 * @throws {Error} If the function is not implemented yet.
 * @returns {string} The latest version number.
 */
function getLatestVersionFromUpstreamRepo(repoType, repoUrl) {
  throw new Error('Not implemented yet')
}

/**
 * Retrieves the latest version of the app.
 *
 * @async
 * @function getLatestVersion
 * @throws {Error} Throws an error if the environment variable APP_UPSTREAM_REPO is not set and NEW_APP_VERSION is not set.
 * @returns {Promise<string>} A promise that resolves to the latest version of the app.
 */
async function getLatestVersion() {

  if (process.env.NEW_APP_VERSION) {
    return process.env.NEW_APP_VERSION;
  }

  const appUpstreamRepo = process.env.APP_UPSTREAM_REPO;
  if (appUpstreamRepo) {

    let repoType = process.env.APP_UPSTREAM_REPO_TYPE

    if (!repoType) {
      repoType = guessRepoTypeFromUrl(appUpstreamRepo);
    }

    return getLatestVersionFromUpstreamRepo(repoType, appUpstreamRepo);

  } else {
    throw new Error('The environment variable APP_UPSTREAM_REPO must be set if NEW_APP_VERSION is not set');
  }

}

/**
 * Entry point of the application
 *
 * @async
 * @function main
 * @returns {Promise<void>}
 */
async function main() {

  // For each file named 'Chart.yaml' in the current directory
  const chartFilePaths = globSync(`${process.cwd()}/**/Chart.yaml`).map(path => relative(process.cwd(), path));

  console.log(`Found ${ chartFilePaths.length } chart files`.blue);

  for (const chartFilePath of chartFilePaths) {
    console.log(`Checking chart file: ${chartFilePath}`.grey);
    const currentVersion = getChartAppVersion(chartFilePath);
    const lastVersion = await getLatestVersion();

    if (currentVersion === lastVersion) {
      console.log('No new version available'.blue);
    } else {
      console.log(`Updating app version from ${ currentVersion } to ${ lastVersion }`.green);
      setChartAppVersion(lastVersion, chartFilePath);
    }
  }

}

main().catch(error => {
  console.error(error.message.red);
  process.exit(1);
});
