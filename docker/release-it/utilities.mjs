
/**
 * Generates tag patterns based on the provided tag name.
 *
 * @param {string} tagName - The tag name to generate patterns for.
 * @throws {Error} Throws an error if the `tagName` option is not provided.
 * @returns {object} An object containing the generated patterns.
 */
export function tagPatterns(tagName) {
  if (!tagName) {
    throw new Error('option git.tagName is required');
  }

  const releasePattern = tagName.replace('${version}', '[0-9]+\\.[0-9]+\\.[0-9]+');
  const tagNamePattern = tagName.replace('${version}', '*.*.*');
  const preReleasePattern = (channel) => `${releasePattern}-${channel}\\.[0-9]+`;
  const preReleaseRegex = (channel) => new RegExp(`^${preReleasePattern(channel)}$`);

  return {
    releasePattern,
    preReleasePattern,
    releaseRegex: new RegExp(`^${releasePattern}$`),
    preReleaseRegex,
    tagNamePattern,
  };
}

export function releasePattern(tagName) {
  return tagPatterns(tagName).releasePattern;
}

export function preReleasePattern(tagName, channel) {
  return tagPatterns(tagName).preReleasePattern(channel);
}

export function releaseRegex(tagName) {
  return tagPatterns(tagName).releaseRegex;
}

export function preReleaseRegex(tagName, channel) {
  return tagPatterns(tagName).preReleaseRegex(channel);
}

export function tagNamePattern(tagName) {
  return tagPatterns(tagName).tagNamePattern;
}
