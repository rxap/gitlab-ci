#!/usr/bin/env node

import release from 'release-it';
import { readFileSync, existsSync } from 'fs';
import { spawn } from 'child_process';
import { tagNamePattern } from './utilities.mjs';
import 'colors';

// region ensure environment variables are set
process.env.RELEASE_IT_INCREMENT ??= 'false';
process.env.RELEASE_IT ??= 'false';
process.env.RELEASE_IT_CHANNEL ??= 'auto';
process.env.RELEASE_IT_PROMOTE ??= 'false';
process.env.RELEASE_IT_DEMOTE ??= 'false';
process.env.RELEASE_IT_FORCE ??= 'false';
process.env.RELEASE_IT_INCREMENT ??= 'false';
// endregion

/**
 * sorted from most stable to least stable
 */
let CHANNELS = [
  ['rc', 'release-candidate'],
  ['preview'],
  ['nightly'],
  ['edge']
];

const DEFAULT_OPTIONS = JSON.parse(readFileSync('/tools/scripts/release-it.json', 'utf8'));

/**
 * Executes a git command with the provided arguments.
 *
 * @param {Array<string>} args - The arguments for the git command.
 * @return {Promise<string>} - A promise that resolves with the output of the git command.
 *                            The output is a string after trimming any leading or trailing whitespace.
 * @throws {Error} - If an error occurs during execution of the git command.
 */
export function git(args) {
  console.log(`$ git ${ args.join(' ') }`.grey);
  return new Promise((resolve, reject) => {
    const s = spawn(
      'git',
      args,
      {
        stdio: [ 'ignore', 'pipe', 'inherit' ],
        shell: true,
      },
    );
    let dataBuffer = '';
    s.on('error', (err) => {
      if (err.code === 'ENOENT') {
        console.log('git must be installed to use the CLI.'.red);
      } else {
        reject(err);
      }
    });
    s.stdout.on('data', (data) => {
      dataBuffer += data.toString('utf-8').trim();
    });
    s.stdout.on('end', () => {
      resolve(dataBuffer.trim());
    });
  });
}

/**
 * Executes the release-cli command with the given arguments.
 *
 * @param {Array<string>} args - The arguments to pass to the release-cli command.
 * @param {boolean} dryRun - Flag to indicate whether to perform a dry run or not.
 * @return {Promise<string>} - A promise that resolves with the output of the release-cli command.
 * If dryRun is true, it resolves with 'dry-run'.
 * If an error occurs during the execution, the promise is rejected with the error.
 */
export function releaseCli(args, dryRun = !process.env.CI || process.env.DRY_RUN) {
  console.log(`$ release-cli ${args.join(' ')}`.grey);
  if (dryRun) {
    return Promise.resolve('dry-run');
  }
  return new Promise((resolve, reject) => {
    const s        = spawn(
      'release-cli',
      args,
      {
        stdio: ['ignore', 'pipe', 'inherit'],
        shell: true
      }
    );
    let dataBuffer = '';
    s.on('error', (err) => {
      if (err.code === 'ENOENT') {
        console.log('release-cli must be installed to use the CLI.'.red);
      } else {
        reject(err);
      }
    });
    s.stdout.on('data', (data) => {
      dataBuffer += data.toString('utf-8').trim();
    });
    s.stdout.on('end', () => {
      resolve(dataBuffer.trim());
    });
  });
}

/**
 * Creates a release on GitLab with the specified tag name, name, and changelog.
 *
 * @param {string} tagName - The tag name for the release.
 * @param {string} name - The name of the release.
 * @param {string} changelog - The changelog for the release.
 * @return {Promise<string>} - A promise that resolves when the release is created.
 */
export async function createGitlabRelease(tagName, name, changelog) {

  const args = [];

  if (process.env.CI_SERVER_URL) {
    args.push(`--server-url ${process.env.CI_SERVER_URL}`)
  }

  if (process.env.CI_JOB_TOKEN) {
    args.push(`--job-token=${process.env.CI_JOB_TOKEN}`)
  }

  if (process.env.CI_PROJECT_ID) {
    args.push(`--project-id=${process.env.CI_PROJECT_ID}`)
  }

  args.push('create');

  args.push(`--tag-name=${tagName}`);
  args.push(`--name=${name}`);
  args.push(`--description="${changelog}"`);

  const response = await releaseCli(args);

  if (response.includes('level=error') || response.includes('level=fatal')) {
    throw new Error('failed to create release');
  }

  return response;
}

/**
 * Retrieves the latest git tag that matches the pattern v*.
 *
 * @returns {Promise<string | null>} The latest git tag.
 */
async function getLatestTag(tagName) {
  const data = await git([ 'tag', '--sort=-taggerdate', '-l', `"${tagNamePattern(tagName)}"`]);
  if (!data) {
    return null;
  }
  const [ latest ] = data.split('\n').map(tag => tag.trim());
  return latest || null;
}

/**
 * Extracts the channel from the provided tag.
 *
 * @param {string} tag - The tag from which to extract the channel.
 * @return {string|null} The extracted channel or null if channel cannot be found.
 */
function extractChannel(tag) {
  if (!tag) {
    throw new Error('unable to extract channel from empty tag');
  }
  const match = tag.match(/.+-(\w+)\./);
  if (match) {
    return match[1];
  }
  return null;
}

/**
 * Retrieves the next channel based on the provided channel.
 * If the channel is the first channel, it returns null, as there is no next channel.
 * If the channel is not the last channel, it returns the preview channel.
 * If the channel is not found, it defaults to the preview channel.
 *
 * @param {string} channel - The channel to find the next channel for.
 * @returns {string|null} - The next channel, or null if there is no next channel.
 */
function getNextChannel(channel) {
  if (!channel) {
    throw new Error('unable to get next channel from empty channel');
  }
  const index = CHANNELS.findIndex(c => c.includes(channel));

  if (index === -1) {
    console.log('channel not found, defaulting to last channel in hierarchy'.yellow);
    return CHANNELS[CHANNELS.length - 1][0];
  }

  if (index === 0) {
    console.log('Current channel is the first channel, the next channel is release'.yellow);
    return null;
  }

  return CHANNELS[index - 1][0];
}

function getPreviousChannel(channel) {
  if (!channel) {
    throw new Error('unable to get previous channel from empty channel');
  }

  const index = CHANNELS.findIndex(c => c.includes(channel));

  if (index === -1) {
    console.log('channel not found, defaulting to last channel in hierarchy'.yellow);
    return CHANNELS[CHANNELS.length - 1][0];
  }

  if (index === CHANNELS.length - 1) {
    console.log('Current channel is the last channel, use this chanel as previous channel'.yellow);
    return CHANNELS[CHANNELS.length - 1][0];
  }

  return CHANNELS[index + 1][0];

}

function getCurrentBranch() {
  return git(['branch', '--show-current']).then(data => data.trim());
}

async function getCurrentRemote() {
  const currentBranch = await getCurrentBranch();
  return git(['config', '--get', `branch.${currentBranch}.remote`]).then(data => data.trim());
}

async function fetchAllTags() {
  const remote = await getCurrentRemote();
  if (!remote) {
    throw new Error('unable to determine remote');
  }
  await git(['fetch', '--tags', '--prune', remote]).then(() => console.log('tags fetched'));
}

async function gitFetch() {
  const remote = await getCurrentRemote();
  if (!remote) {
    throw new Error('unable to determine remote');
  }
  await git(['fetch', remote]).then(() => console.log('fetched'));
}

async function hasChangesSinceLastTag(tagName) {

  const latestTag = await getLatestTag(tagName);

  if (!latestTag) {
    return true;
  }

  const commitCount = Number(await git([ 'rev-list', '--count', `${latestTag}..HEAD` ]));

  if (isNaN(commitCount)) {
    throw new Error('unable to determine commit count');
  }

  return commitCount > 0;
}

function loadReleaseItConfig() {

  if (existsSync('.release-it.json')) {
    console.log('release-it configuration found in .release-it.json'.grey);
    return JSON.parse(readFileSync('./.release-it.json', 'utf8'));
  }

  if (existsSync('package.json')) {
    const packageJson = JSON.parse(readFileSync('package.json', 'utf8'));
    if (packageJson['release-it']) {
      console.log('release-it configuration found in package.json'.grey);
      return packageJson['release-it'];
    }
  }

  console.log('no release-it configuration found');
  return {};

}

async function buildOptions() {

  // TODO : deep merge the options
  const options = { ...DEFAULT_OPTIONS, ...loadReleaseItConfig() };

  // set default channels
  options.channels ??= CHANNELS;
  // set global channels
  CHANNELS = options.channels;

  options.plugins ??= {};
  options.plugins['/tools/scripts/rxap-release-it-plugin.mjs'] = {
    preReleaseHierarchy: CHANNELS.map(c => c[0]),
  };
  options.preReleaseHierarchy = CHANNELS.map(c => c[0]);

  options.git ??= {};

  if (!process.env.CI) {
    if (process.env.RELEASE_IT !== 'true'){
      console.log('RELEASE_IT is not set to true'.red)
      process.exit(1);
    }
  }

  if (!process.env.CI || process.env.DRY_RUN) {
    console.log('running in dry-run mode'.magenta);
    options['dry-run'] = true;
    if (!process.env.CI) {
      options.git.requireCleanWorkingDir = false;
    }
  }

  options.ci = true;
  options.debug = true;

  if (process.env.RELEASE_IT_VERBOSE) {
    options.verbose = true;
  }
  if (!isNaN(Number(process.env.RELEASE_IT_VERBOSITY_LEVEL))) {
    const verbosityLevel = Number(Number(process.env.RELEASE_IT_VERBOSITY_LEVEL).toFixed(0));
    if (verbosityLevel < 0 || verbosityLevel > 2) {
      console.warn(`invalid verbosity level '${verbosityLevel}', defaulting to 1`.yellow);
    }
    switch (options.verbose) {
      case 0:
        options.verbose = false;
        break;
      case 1:
        options.verbose = true;
        break;
      case 2:
        options.verbose = 2;
        break;
    }
  }

  switch (process.env.RELEASE_IT_CHANNEL) {
    case 'release':
      console.log('release channel');
      options.preRelease = null;
      break;
    case 'auto':
      console.log('automatic channel detection');
      const latestTag = await getLatestTag(options.git.tagName);
      let channel;
      if (latestTag) {
        console.log('latest tag:', latestTag);
        channel = extractChannel(latestTag);
        if (!channel) {
          channel = CHANNELS[CHANNELS.length - 1][0];
          console.log(`no channel found, default to ${channel} channel`);
        } else {
          console.log('channel:', channel);
          if (process.env.RELEASE_IT_PROMOTE === 'true') {
            console.log('promoting')
            channel = getNextChannel(channel);
            console.log(`promoting to '${channel}'`)
          } else if (process.env.RELEASE_IT_DEMOTE === 'true') {
            console.log('demoting')
            channel = getPreviousChannel(channel);
            console.log(`demoting to '${channel}'`)
          } else if (!channel) {
            // if the latest tag has no channel, default to preview
            console.log('no channel found, defaulting to preview');
            channel = 'preview';
          }
        }
      } else {
        channel = CHANNELS[CHANNELS.length - 1][0];
      }
      console.log(`setting pre-release to '${channel}'`);
      options.preRelease = channel;
      break;
    default:
      if (!process.env.RELEASE_IT_CHANNEL) {
        console.log('no channel found'.yellow);
        const latestTag = await getLatestTag(options.git.tagName);
        let currentChannel = CHANNELS[CHANNELS.length - 1][0];
        if (latestTag) {
          console.log(`latest tag: ${ latestTag }`);
          currentChannel = extractChannel(latestTag);
        }
        console.log(`current channel: ${ currentChannel }`);
        process.env.RELEASE_IT_CHANNEL = currentChannel;
      }
      if (CHANNELS.some(c => c.includes(process.env.RELEASE_IT_CHANNEL))) {
        for (const channel of CHANNELS) {
          if (channel.includes(process.env.RELEASE_IT_CHANNEL)) {
            console.log(`setting pre-release to '${ channel[0] }'`.blue);
            options.preRelease = channel[0];
            break;
          }
        }
      } else {
        console.log(`non standard channel: ${process.env.RELEASE_IT_CHANNEL}`.yellow);
        options.preRelease = process.env.RELEASE_IT_CHANNEL;
      }
      break;

  }

  if (process.env.RELEASE_IT_INCREMENT !== 'false') {
    console.log(`incrementing version by ${process.env.RELEASE_IT_INCREMENT}`)
    // ensure that the plugin is not be used to determine the next version based
    // on the commit messages, but the version bump is determined by the environment variable
    options.plugins['@release-it/conventional-changelog'].ignoreRecommendedBump = true;
    switch (process.env.RELEASE_IT_INCREMENT) {
      case 'patch':
        options.version ??= {};
        options.version.increment = 'patch';
        break;
      case 'minor':
        options.version ??= {};
        options.version.increment = 'minor';
        break;
      case 'major':
        options.version ??= {};
        options.version.increment = 'major';
        break;
    }
  }

  if (options.preRelease) {
    console.log('pre-release found'.blue);
  } else {
    console.log('no pre-release found, defaulting to release'.yellow);
  }

  return options;

}

async function checkPrerequisites() {
  const gitVersion = await git(['--version']);
  console.log('git version:', gitVersion)
  const releaseCliVersion = await releaseCli(['--version'])
  if (!releaseCliVersion.match(/release-cli version/)) {
    console.log(`release-cli version valid found: ${ releaseCliVersion }`.red);
    if (!(!process.env.CI || process.env.DRY_RUN)) {
      process.exit(1);
    }
  } else {
    console.log(`release-cli version: ${ releaseCliVersion }`.cyan)
  }
}

function buildTagName(version, options) {
  if (options.git?.tagName) {
    return options.git.tagName.replace('${version}', version);
  }
  return version;
}

function buildGitlabReleaseName(version, options) {
  if (options.gitlab?.releaseName) {
    return options.gitlab.releaseName.replace('${version}', version);
  }
  return version;
}

async function checkIfHeadIsLatestCommitOfTheRemote() {
  const currentRemote = await getCurrentRemote();
  const currentBranch = await getCurrentBranch();
  const commitCount = Number(await git([ 'rev-list', '--count', `HEAD..${currentRemote}/${currentBranch}` ]));

  return commitCount === 0;
}

async function shouldANewReleaseBeCreated(tagName, preRelease) {

  const hasChanges = await hasChangesSinceLastTag(tagName);

  if (hasChanges) {
    console.log('changes found since last tag'.blue);
    return true;
  }

  const lastRelease = await getLatestTag(tagName);

  if (!lastRelease) {
    console.log('no previous release found'.blue);
    return true;
  }

  const currentChannel = extractChannel(lastRelease);

  if (currentChannel === null && !preRelease) {
    // the current chanel is not a pre release and the target channel is also not a pre release
    console.log('no changes since last tag, skipping release'.yellow);
    return false;
  }

  if (currentChannel === preRelease) {
    console.log('unchanged channel and no changes since last tag, skipping release'.yellow);
    return false;
  }

  // check if the current channel is before the target channel in the hierarchy
  const currentChannelIndex = CHANNELS.findIndex(c => c.includes(currentChannel));

  if (!preRelease) {
    console.log('Target is release, and the current channel is a pre-release, so we need to release'.blue)
    return true;
  }

  const targetChannelIndex = CHANNELS.findIndex(c => c.includes(preRelease));

  if (targetChannelIndex === -1) {
    console.log('Custom channel and no changes since last tag, skipping release'.yellow);
    return false;
  }

  if (targetChannelIndex >= currentChannelIndex) {
    console.log('target channel is not before the current channel in the hierarchy and no changes since last tag, skipping release'.yellow);
    return false;
  }

  console.log('target channel is before the current channel in the hierarchy'.blue)
  return true;

}

async function main() {

  await checkPrerequisites();

  console.log('fetching remote'.grey);
  await gitFetch();

  console.log('fetching tags'.grey);
  await fetchAllTags();

  const isHeadLatestCommit = await checkIfHeadIsLatestCommitOfTheRemote();

  if (!isHeadLatestCommit) {
    console.log('HEAD is not the latest commit of the remote'.red);
    process.exit(1);
  }

  console.log('building options'.grey)
  const options = await buildOptions();

  if (!(await shouldANewReleaseBeCreated(options.git.tagName, options.preRelease))) {
    if (process.env.RELEASE_IT_FORCE !== 'true' && !(process.env.RELEASE_IT_INCREMENT !== 'false')) {
      console.log('no changes since last tag, skipping release'.yellow);
      process.exit(0);
    }
  }

  console.log('options', JSON.stringify(options, undefined, 2));

  const { version, changelog } = await release(options).then(output => {
    // { version, latestVersion, name, changelog }
    console.log(`release succeeded. ${output.latestVersion}...${output.version}`.blue);
    console.log('changelog:\n'.grey);
    console.log(`${output.changelog}`.grey)
    console.log('output:\n'.grey);
    console.log(`${JSON.stringify(output)}`.grey);
    return output;
  });

  await createGitlabRelease(
    buildTagName(version, options),
    buildGitlabReleaseName(version, options),
    changelog
  ).then(() => console.log('gitlab release created'.blue));

}

main().then(() => console.log('done')).catch(e => {
  console.log('failed:', e.message, e.stack);
  process.exit(1);
});


