import { Plugin } from 'release-it';
import {
  preReleaseRegex,
  releaseRegex,
  tagNamePattern,
} from './utilities.mjs';
import 'colors';
import { rsort } from 'semver';

const options = { write: false };

class RxapPlugin extends Plugin {

  async getIncrement() {
    if (process.env.RELEASE_IT_INCREMENT !== 'false') {
      return process.env.RELEASE_IT_INCREMENT;
    }
  }

  async getLatestVersion() {

    const tagName = this.config.options.git.tagName;

    const pattern = tagNamePattern(tagName);
    console.log(`rxap getLatestVersion with pattern: ${pattern}`.grey);
    const tagList = await this.exec(`git tag --sort=-taggerdate -l "${pattern}"`, { options }).then(
      stdout => {
        if (stdout) {
          return stdout.split('\n');
        }
        return null;
      },
      () => null
    );

    if (tagList?.length) {
      let patternList = [releaseRegex(tagName)];
      if (this.config.options.preRelease) {
        const hierarchy = this.options.preReleaseHierarchy;
        const channel = this.config.options.preRelease;
        let index = hierarchy.indexOf(channel);
        if (index === -1) {
          index = hierarchy.length;
        } else {
          index++;
        }
        const includedChannelList = [this.config.options.preRelease];
        if (process.env.RELEASE_IT_PROMOTE === 'true') {
          console.log('in promote mode'.blue)
          includedChannelList.push(...hierarchy.slice(0, index));
        } else if (process.env.RELEASE_IT_DEMOTE === 'true') {
          console.log('in demote mode'.blue)
          includedChannelList.push(...hierarchy.slice(index));
        } else {
          console.log('in normal mode'.blue)
        }
        console.log(`include pre-release names: ${ includedChannelList.join(', ') } for channel: ${ channel }`.cyan);
        patternList.push(...includedChannelList.map(channel => preReleaseRegex(tagName, channel)));
      } else {
        console.log('release only'.blue);
      }
      const latestVersion = this.firstThatMatch(tagList, patternList);
      if (latestVersion) {
        this.config.setContext({ latestTag: latestVersion });
      }
      return latestVersion;
    } else {
      console.log('no tags found'.red);
    }

    return null;

  }

  firstThatMatch(tagList, patternList) {
    tagList = rsort(tagList);
    const shortTagList = tagList.slice(0, 10);
    console.log(`checking tags: ${ shortTagList.join(', ') } ...`.grey)
    console.log(`with patterns: ${ patternList.join(', ') }`.grey)
    for (const tag of tagList) {
      console.log(`checking tag: ${ tag }`.grey);
      if (patternList.some(pattern => pattern.test(tag))) {
        console.log(`found latest tag: ${ tag }`.green);
        return tag;
      }
    }
    console.log('no matching tag found'.yellow);
    return null;
  }


}

export default RxapPlugin;
